package physicaltestmanager.test.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.PhysicalTest;
import physicaltestmanager.test.dao.PhysicalTestRepository;

/**
 * A fizikai állapot felméréseket tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class PhysicalTestRepositoryJDBCImpl implements PhysicalTestRepository {

    private Connection conn;
    private PreparedStatement findAll;
    private PreparedStatement insertTest;
    private PreparedStatement updateTest;
    private PreparedStatement deleteTest;
    private PreparedStatement findByMilitaryId;
    private PreparedStatement findByYear;
    private PreparedStatement findById;

    public PhysicalTestRepositoryJDBCImpl(Connection conn) throws PhysicalTestManagerDAOException {
    	this.conn = conn;
    }
    
    @Override
    public List<PhysicalTest> findAll() throws PhysicalTestManagerDAOException {
        try {
        	findAll = conn.prepareStatement("SELECT * FROM physical_tests");
            ResultSet all = findAll.executeQuery();
            List<PhysicalTest> list = makeList(all);
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<PhysicalTest> findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        try {
        	findByMilitaryId = conn.prepareStatement("SELECT * FROM physical_tests WHERE military_id = ?");
            findByMilitaryId.setInt(1, militaryId);
            ResultSet rsMilitaryId = findByMilitaryId.executeQuery();
            List<PhysicalTest> list = makeList(rsMilitaryId);
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<PhysicalTest> findByYear(int year) throws PhysicalTestManagerDAOException {
        try {
        	findByYear = conn.prepareStatement("SELECT * FROM physical_tests WHERE date_of_execution > ? AND date_of_execution < ?");
            LocalDate startDate = LocalDate.of(year, 01, 01);
            LocalDate endDate = LocalDate.of(year + 1, 01, 01);
            findByYear.setDate(1, Date.valueOf(startDate));
            findByYear.setDate(2, Date.valueOf(endDate));
            ResultSet rsYear = findByYear.executeQuery();
            List<PhysicalTest> list = makeList(rsYear);
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
    
    @Override
    public PhysicalTest findById(int id) throws PhysicalTestManagerDAOException {
        try {
        	findById= conn.prepareStatement("SELECT * FROM physical_tests WHERE id = ?");
            findById.setInt(1, id);
            ResultSet rsId = findById.executeQuery();
            rsId.next();
            PhysicalTest test = makeOne(rsId);
            return test;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public void saveTest(PhysicalTest test) throws PhysicalTestManagerDAOException {
        if (test.getId() == null) {
            insertTest(test);
        } else {
            updateTest(test);
        }
    }
    
    @Override
    public void insertTest(PhysicalTest test) throws PhysicalTestManagerDAOException {
        try {
        	insertTest = conn.prepareStatement("INSERT INTO physical_tests (military_id, date_of_execution, "
        			+ "arm_form_of_movement, arm_result, trunk_form_of_movement, trunk_result, circulation_form_of_movement, "
        			+ "circulation_result, score, bmi, body_fat_index) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
            insertTest.setInt(1, test.getMilitaryId());
            insertTest.setDate(2, Date.valueOf(test.getDateOfExecution()));
            insertTest.setInt(3, test.getArmFormOfMovement());
            insertTest.setInt(4, test.getArmResult());
            insertTest.setInt(5, test.getTrunkFormOfMovement());
            insertTest.setInt(6, test.getTrunkResult());
            insertTest.setInt(7, test.getCirculationFormOfMovement());
            insertTest.setInt(8, test.getCirculationResult());
            insertTest.setInt(9, test.getScore());
            insertTest.setString(10, test.getBmi());
            insertTest.setString(11, test.getBodyFatIndex());
            insertTest.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public void updateTest(PhysicalTest test) throws PhysicalTestManagerDAOException {
        try {
        	updateTest = conn.prepareStatement("UPDATE physical_tests SET military_id = ?, date_of_execution = ?,"
        			+ " arm_form_of_movement = ?, arm_result = ?, trunk_form_of_movement = ?, trunk_result = ?,"
        			+ " circulation_form_of_movement = ?, circulation_result = ?, score = ?, bmi = ?, body_fat_index = ? WHERE id = ?");
            updateTest.setInt(1, test.getMilitaryId());
            updateTest.setDate(2, Date.valueOf(test.getDateOfExecution()));
            updateTest.setInt(3, test.getArmFormOfMovement());
            updateTest.setInt(4, test.getArmResult());
            updateTest.setInt(5, test.getTrunkFormOfMovement());
            updateTest.setInt(6, test.getTrunkResult());
            updateTest.setInt(7, test.getCirculationFormOfMovement());
            updateTest.setInt(8, test.getCirculationResult());
            updateTest.setInt(9, test.getScore());
            updateTest.setString(10, test.getBmi());
            updateTest.setString(11, test.getBodyFatIndex());
            updateTest.setInt(12, test.getId());
            updateTest.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
    
    @Override
    public void deleteTest(int id) throws PhysicalTestManagerDAOException {
        try {
        	deleteTest = conn.prepareStatement("DELETE FROM physical_tests WHERE id = ?");
            deleteTest.setInt(1, id);
            deleteTest.executeUpdate();
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
    
    /**
     * Segédmetódus, ami felmérés objektumokból egy listát készít.
     */
    private List<PhysicalTest> makeList(ResultSet rs) throws PhysicalTestManagerDAOException {
        try {
            List<PhysicalTest> list = new ArrayList<>();
            while (rs.next()) {
                list.add(makeOne(rs));
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    /**
     * Segédmetódus, ami egy felmérés objektumot készít.
     */
    private PhysicalTest makeOne(ResultSet rs) throws PhysicalTestManagerDAOException {
        try {
            PhysicalTest test = new PhysicalTest();
            test.setId(rs.getInt("id"));
            test.setMilitaryId(rs.getInt("military_id"));
            test.setDateOfExecution(rs.getDate("date_of_execution").toLocalDate());
            test.setArmFormOfMovement(rs.getInt("arm_form_of_movement"));
            test.setArmResult(rs.getInt("arm_result"));
            test.setTrunkFormOfMovement(rs.getInt("trunk_form_of_movement"));
            test.setTrunkResult(rs.getInt("trunk_result"));
            test.setCirculationFormOfMovement(rs.getInt("circulation_form_of_movement"));
            test.setCirculationResult(rs.getInt("circulation_result"));
            test.setScore(rs.getInt("score"));
            test.setBmi(rs.getString("bmi"));
            test.setBodyFatIndex(rs.getString("body_fat_index"));
            return test;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
}
