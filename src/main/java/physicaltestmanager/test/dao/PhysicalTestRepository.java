package physicaltestmanager.test.dao;

import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.PhysicalTest;

/**
 * A fizikai állapot felméréseket tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface PhysicalTestRepository {
    
    /**
     * Az összes felmérés lekérdezésére szolgáló metódus.
     *
     * @return Visszaadja az összes felmérést tartalmazó listát.
     */
    List<PhysicalTest> findAll() throws PhysicalTestManagerDAOException;
    
    /**
     * SZTSZ szám szerint megkeresi a felméréseket.
     *
     * @param militaryId Az SZTSZ szám, ami alapján keressük a felméréseket.
     * @return Visszaadja a felmérések listáját.
     */
    List<PhysicalTest> findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException;
    
    /**
     * Kilistázza az adott évben végrehajtott felméréseket.
     *
     * @param year Az év, ami alapján keressük a felméréseket.
     * @return Visszaadja a felmérések listáját.
     */
    List<PhysicalTest> findByYear(int year) throws PhysicalTestManagerDAOException;
    
    /**
     * Sorszám szerint megkeresi a felmérést.
     *
     * @param id A sorszam, ami alapján keressük a felmérést.
     * @return Visszaadja a felmérést.
     */
    PhysicalTest findById(int id) throws PhysicalTestManagerDAOException;
    
    /**
     * Elmenti az adatbázisba a felmérés objektumot. Ha nem létezik még a felmérés
     * az adatbázisban, akkor újat ad hozzá, ha létezik, akkor felülírja a
     * meglévőt.
     *
     * @param test A felmérés, amit el akarunk menteni az adatbázisba.
     */
    void saveTest(PhysicalTest test) throws PhysicalTestManagerDAOException;
    
    /**
     * Új felmérés felvitele az adatbázisba.
     */
    void insertTest(PhysicalTest test) throws PhysicalTestManagerDAOException;
    
    /**
     * Felmérés adatainak frissítése az adatbázisban.
     */
    void updateTest(PhysicalTest test) throws PhysicalTestManagerDAOException;
    
    /**
     * Töröl egy felmérést a megadott sorszám alapján az adatbázisból.
     */
    void deleteTest(int id) throws PhysicalTestManagerDAOException;
    
}
