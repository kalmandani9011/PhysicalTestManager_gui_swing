package physicaltestmanager.controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.gui.OverviewDialog;
import physicaltestmanager.gui.PersonDialog;
import physicaltestmanager.gui.PhysicalTestDialog;
import physicaltestmanager.gui.SearchDialog;
import physicaltestmanager.model.Person;
import physicaltestmanager.model.PhysicalTest;
import physicaltestmanager.service.ScoreCalculatorService;
import physicaltestmanager.util.ScoreCalculatorUtil;

/**
 * A GUI és a pontszámító service osztály közötti kapcsolatot teremti meg.
 */
public class GuiController {

    private static final int DEFAULTNULLVALUE = -1;
    private ScoreCalculatorService calculator;
    private DefaultListModel<Person> model;
    private DefaultTableModel database;
    private Person person;
    private List<Person> persons;
    private PersonDialog personDialog;
    private PhysicalTestDialog testDialog;
    private OverviewDialog overviewDialog;
    private Integer selectedItemIndex = DEFAULTNULLVALUE;
    private List<Integer> militaryIdList = new ArrayList<>();
    private PhysicalTest test;
    

    public GuiController() throws PhysicalTestManagerDAOException {
		calculator = ScoreCalculatorUtil.getInstance();
        persons = calculator.getPersonsWithTests();
        loadMilitaryIds();
    }

    private void loadMilitaryIds() {
        militaryIdList.clear();
        for (int i = 0; i < persons.size(); i++) {
            militaryIdList.add(persons.get(i).getMilitaryId());
        }
    }

    public void setModel(DefaultListModel model) {
        this.model = model;
    }

    public void setDatabaseModel(DefaultTableModel database) {
        this.database = database;
    }

    public void setSelectedItemIndex(Integer index) {
        this.selectedItemIndex = index;
    }

    /**
     * A lista elemeinek újratöltése.
     */
    public void sortList() {
        for (Person p : persons) {
            model.addElement(p);
        }
    }

    /**
     * A megadott Comparatorral rendezi a személyek lista elemeit.
     */
    public void sort(Comparator comp) {
        persons.sort(comp);
        model.clear();
        sortList();
    }
    
    /**
     * Új személy felvitele külön dialógus ablak segítségével.
     */
    public void showPersonDialog() throws PhysicalTestManagerDAOException {
        personDialog = new PersonDialog(null);
        personDialog.setMilitaryIds(militaryIdList);
        Optional<Person> optPerson = personDialog.showDialog();
        if (optPerson.isPresent()) {
            person = optPerson.get();
            addPerson();
        }
    }

    /**
     * Kiválasztott személy adatainak módosítása külön dialógus ablak
     * segítségével.
     */
    public void modifyPerson() throws PhysicalTestManagerDAOException {
        if (selectedItemIndex != DEFAULTNULLVALUE) {
            personDialog = new PersonDialog(null, Optional.of(model.get(selectedItemIndex)));
            personDialog.setMilitaryIds(militaryIdList);
            Optional<Person> optPerson = personDialog.showDialog();
            if (optPerson.isPresent()) {
                person = optPerson.get();
                addPerson();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }
    
    /**
     * Segédmetódus a személy lista és a személy adatbázis elemeinek
     * módosítására illetve új személy hozzáadására.
     */
    private void addPerson() throws PhysicalTestManagerDAOException {
        calculator.savePerson(person);
        if (person.getId() == null) {
            Integer id = calculator.findPersonByMilitaryId(person.getMilitaryId()).getId();
            person.setId(id);
            persons.add(person);
            model.addElement(person);
        } else {
            persons.set(selectedItemIndex, person);
            model.set(selectedItemIndex, person);
        }
        loadMilitaryIds();
        loadAll();
    }

    /**
     * Személy törlése a listából és az adatbázisból.
     */
    public void deletePerson(Person p) throws PhysicalTestManagerDAOException {
        try {
            if (selectedItemIndex != DEFAULTNULLVALUE) {
                JPanel panel = new JPanel();
                panel.add(new JLabel("Biztosan törölni szeretnéd a kiválasztott elemet?"));
                int result = JOptionPane.showConfirmDialog(null, panel, "Törlés", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    calculator.deletePerson(p.getId());
                    persons.remove(selectedItemIndex.intValue());
                    model.remove(selectedItemIndex);
                    loadAll();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
            }
        } catch (PhysicalTestManagerDAOException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    /**
     * Fizikai felmérés felvitele a kiválasztott személyhez.
     */
    public void showTestDialog() throws PhysicalTestManagerDAOException {
        if (selectedItemIndex != DEFAULTNULLVALUE) {
            person = model.get(selectedItemIndex);
            testDialog = new PhysicalTestDialog(null, person);
            Optional<PhysicalTest> optTest = testDialog.showDialog();
            if (optTest.isPresent()) {
                test = optTest.get();
                addTest();
                if (overviewDialog != null && overviewDialog.isVisible()) {
                    overviewDialog.setVisible(false);
                    testOverview();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }
    
    /**
     * A kijelölt felmérés módosítására szolgáló metódus.
     *
     * @param id A kijelölt felmérés listában elfoglalt sorának száma.
     */
    public void modifyTest(int id) throws PhysicalTestManagerDAOException {
        person = model.get(selectedItemIndex);
        testDialog = new PhysicalTestDialog(null, person, Optional.of(person.getTests().get(id)));
        Optional<PhysicalTest> optTest = testDialog.showDialog();
        if (optTest.isPresent()) {
            test = optTest.get();
            addTest();
            overviewDialog.setVisible(false);
            testOverview();
        }
    }
    
    /**
     * Egy kijelölt személy fizikai felméréseinek kilistázása külön ablakban.
     */
    public void testOverview() throws PhysicalTestManagerDAOException {
        if (selectedItemIndex != DEFAULTNULLVALUE) {
            person = model.get(selectedItemIndex);
            if (person.getTests().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Nincs a személyhez tartozó felmérés!", "Hiba!", JOptionPane.OK_OPTION);
                return;
            }
            overviewDialog = new OverviewDialog(null, person, this);
            overviewDialog.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Nincs személy kijelölve!", "Hiba!", JOptionPane.OK_OPTION);
        }
    }

    /**
     * Felmérés törlése az adatbázisból.
     *
     * @param id A felmérés adatbázis szerinti id-ja.
     */
    public void deleteTest(int id) throws PhysicalTestManagerDAOException {
        JPanel panel = new JPanel();
        panel.add(new JLabel("Biztosan törölni szeretnéd a kiválasztott elemet?"));
        int result = JOptionPane.showConfirmDialog(null, panel, "Törlés", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            person = model.get(selectedItemIndex);
            calculator.deleteTest(id);
            calculator.refreshTest(person);
            model.set(selectedItemIndex, person);
            persons.set(selectedItemIndex, person);
            loadAll();
            overviewDialog.setVisible(false);
            testOverview();
        }
    }
    
    /**
     * Segédmetódus az adatbázisban és a személyeknél szereplő felmérések módosítására, vagy új
     * létrehozására.
     */
    private void addTest() throws PhysicalTestManagerDAOException {
        calculator.saveTestAndBindToPerson(person, test);
        persons.set(selectedItemIndex, person);
        loadAll();
    }

    /**
     * Keresést hajt végre az adatbázisban név vagy SZTSZ szám szerint.
     *
     * @return Visszaadja a keresett személy indexét a listában.
     */
    public int search() throws PhysicalTestManagerDAOException {
        SearchDialog searchDialog = new SearchDialog(null, this);
        Optional<String> optSearch = searchDialog.showDialog();
        if (optSearch == null) {
            return -2;
        }
        if (optSearch.isPresent()) {
            String search = optSearch.get();
            if (search.matches("[0-9]+")) {
                int militaryId = Integer.parseInt(search);
                for (Person p : persons) {
                    if (p.getMilitaryId() == militaryId) {
                        return persons.indexOf(p);
                    }
                }
            } else {
                for (Person p : persons) {
                    if (p.getName().equalsIgnoreCase(search)) {
                        return persons.indexOf(p);
                    }
                }
            }
        }
        return -1;
    }

    /**
     * A program adatbázis fülén lévő listába betölti az összes személyt a hozzájuk
     * tartozó felméréseket.
     */
    public void loadAll() {
        database.setRowCount(0);
        for (Person p : persons) {
            if (p.getTests().isEmpty()) {
                database.addRow(newPersonRow(p).toArray());
            } else {
                for (PhysicalTest test : p.getTests()) {
                    List<Object> newRow = newPersonRow(p);
                    newRow.addAll(newTestRow(test));
                    database.addRow(newRow.toArray());
                }
            }
        }
    }

    /**
     * A program adatbázis fülén lévő listába tölti az összes felmérést az adott évben.
     */
    public void loadSelectedYear(int year) throws PhysicalTestManagerDAOException {
        database.setRowCount(0);
        for (Person p : persons) {
            List<PhysicalTest> tests = calculator.findTestByYear(year);
            for (PhysicalTest f : tests) {
                if (f.getMilitaryId() == p.getMilitaryId()) {
                    List<Object> newRow = newPersonRow(p);
                    newRow.addAll(newTestRow(f));
                    database.addRow(newRow.toArray());
                }
            }
        }
    }

    /**
     * A program adatbázis fülén lévő listába tölti azokat a személyeket, akiknek van felmérésük.
     */
    public void loadHasTest() {
        database.setRowCount(0);
        for (Person p : persons) {
            if (!p.getTests().isEmpty()) {
                for (PhysicalTest test : p.getTests()) {
                    List<Object> newRow = newPersonRow(p);
                    newRow.addAll(newTestRow(test));
                    database.addRow(newRow.toArray());
                }
            }
        }
    }

    private List<Object> newPersonRow(Person p) {
        List<Object> newRow = new ArrayList<>();
        newRow.add(p.getId());
        newRow.add(p.getMilitaryId());
        newRow.add(p.getName());
        newRow.add(p.getRank());
        newRow.add(p.getUnit());
        newRow.add(p.getBirthDate());
        newRow.add(p.getMothersMaidenName());
        newRow.add(p.isMale() ? "férfi" : "nő");
        newRow.add(p.getHeight());
        newRow.add(p.getWeight());
        newRow.add(p.getBodyFat());
        return newRow;
    }

    private List<Object> newTestRow(PhysicalTest test) {
        List<Object> newRow = new ArrayList<>();
        newRow.add(test.getDateOfExecution());
        newRow.add(getArmFormOfMovement(test.getArmFormOfMovement()));
        newRow.add(test.getArmResult());
        newRow.add(getTrunkFormOfMovement(test.getTrunkFormOfMovement()));
        newRow.add(test.getTrunkResult());
        newRow.add(getCirculationFormOfMovement(test.getCirculationFormOfMovement()));
        newRow.add(test.getCirculationResult());
        newRow.add(test.getScore());
        newRow.add(test.getBmi());
        newRow.add(test.getBodyFatIndex());
        return newRow;
    }

    private String getArmFormOfMovement(int code) {
        String s = "";
        switch (code) {
            case 0:
                s = "fekvőtámasz";
                break;
            case 1:
                s = "húzódzkodás";
                break;
            case 2:
                s = "hajlított karú függés";
                break;
        }
        return s;
    }

    private String getTrunkFormOfMovement(int code) {
        String s = "";
        switch (code) {
            case 0:
                s = "felülés";
                break;
            case 1:
                s = "lapocka emelés";
                break;
            case 2:
                s = "függő térdemelés";
                break;
        }
        return s;
    }

    private String getCirculationFormOfMovement(int code) {
        String s = "";
        switch (code) {
            case 0:
                s = "3200m futás";
                break;
            case 1:
                s = "2000m futás";
                break;
            case 2:
                s = "1600m menet";
                break;
            case 3:
                s = "ergometria";
                break;
        }
        return s;
    }
}
