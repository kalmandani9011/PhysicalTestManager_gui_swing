package physicaltestmanager.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.service.ScoreCalculatorService;

/**
 *
 * @author
 */
public class ScoreCalculatorUtil {
    
    private static final ScoreCalculatorService calculator;
    
    static {
        try {
        	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/physical_test_table?serverTimezone=UTC", "root", "1234");
            calculator = new ScoreCalculatorService(conn);
        } catch (PhysicalTestManagerDAOException | SQLException ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static ScoreCalculatorService getInstance() {
        return calculator;
    }
}
