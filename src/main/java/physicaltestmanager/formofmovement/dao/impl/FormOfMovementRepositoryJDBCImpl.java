package physicaltestmanager.formofmovement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.formofmovement.dao.FormOfMovementRepository;
import physicaltestmanager.model.ArmFormOfMovement;
import physicaltestmanager.model.BMI;
import physicaltestmanager.model.BentArmHang;
import physicaltestmanager.model.CirculationFormOfMovement;
import physicaltestmanager.model.Crunch;
import physicaltestmanager.model.Ergometry;
import physicaltestmanager.model.KneeRaise;
import physicaltestmanager.model.PullUp;
import physicaltestmanager.model.PushUp;
import physicaltestmanager.model.Running2000;
import physicaltestmanager.model.Running3200;
import physicaltestmanager.model.SitUp;
import physicaltestmanager.model.TrunkFormOfMovement;
import physicaltestmanager.model.Walk1600;

/**
 * Az alapadatokat tartalmazó adatbázissal való kommunikációra alkalmas osztály.
 */
public class FormOfMovementRepositoryJDBCImpl implements FormOfMovementRepository {

    private static final int SITUPCOMBOBOXELEMENT = 0;
    private static final int CRUNCHCOMBOBOXELEMENT = 1;
    private static final int KNEERAISECOMBOBOXELEMENT = 2;
    private Connection conn;
    private PreparedStatement pushUpQuery;
    private PreparedStatement pullUpQuery;
    private PreparedStatement bentArmHangQuery;
    private PreparedStatement sitUpQuery;
    private PreparedStatement crunchQuery;
    private PreparedStatement kneeRaiseQuery;
    private PreparedStatement running3200Query;
    private PreparedStatement running2000Query;
    private PreparedStatement walk1600Query;
    private PreparedStatement ergometryQuery;
    private PreparedStatement bmiQuery;
    private Map<String, PreparedStatement> queries;

    public FormOfMovementRepositoryJDBCImpl(Connection conn) throws PhysicalTestManagerDAOException {
    	try {
    		this.conn = conn;
    		createPreparedStatements();
    	} catch (SQLException ex) {
    		throw new PhysicalTestManagerDAOException(ex.getMessage());
    	}
    }

    @Override
    public int findScore(String table, int result, String ageGroup) throws PhysicalTestManagerDAOException {
        try {
            ResultSet rs;
            queries.get(table).setInt(1, result);
            rs = queries.get(table).executeQuery();
            rs.next();
            return rs.getInt(ageGroup);
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
    
    private void createPreparedStatements() throws SQLException {
		pushUpQuery = conn.prepareStatement("SELECT * FROM push_up WHERE repetition = ?");
        pullUpQuery = conn.prepareStatement("SELECT * FROM pull_up WHERE repetition = ?");
        bentArmHangQuery = conn.prepareStatement("SELECT * FROM bent_arm_hang WHERE time = ?");
        sitUpQuery = conn.prepareStatement("SELECT * FROM sit_up WHERE repetition = ?");
        crunchQuery = conn.prepareStatement("SELECT * FROM crunch WHERE repetition = ?");
        kneeRaiseQuery = conn.prepareStatement("SELECT * FROM knee_raise WHERE repetition = ?");
        running3200Query = conn.prepareStatement("SELECT * FROM running3200 WHERE time = ?");
        running2000Query = conn.prepareStatement("SELECT * FROM running2000 WHERE time = ?");
        walk1600Query = conn.prepareStatement("SELECT * FROM walk1600 WHERE time = ?");
        ergometryQuery = conn.prepareStatement("SELECT * FROM ergometry WHERE wattkg = ?");
        bmiQuery = conn.prepareStatement("SELECT * FROM bmi WHERE height = ?");
        queries = new HashMap<>();
        queries.put("pushUp", pushUpQuery);
        queries.put("pullUp", pullUpQuery);
        queries.put("hang", bentArmHangQuery);
        queries.put("sitUp", sitUpQuery);
        queries.put("crunch", crunchQuery);
        queries.put("kneeRaise", kneeRaiseQuery);
        queries.put("running3200", running3200Query);
        queries.put("running2000", running2000Query);
        queries.put("walk1600", walk1600Query);
        queries.put("ergometry", ergometryQuery);
    }

    @Override
    public int[] findBMIScore(int height, String ageGroup) throws PhysicalTestManagerDAOException {
        try {
        	bmiQuery = conn.prepareStatement("SELECT * FROM bmi WHERE height = ?");
            ResultSet rs;
            bmiQuery.setInt(1, height);
            rs = bmiQuery.executeQuery();
            rs.next();
            return new int[]{rs.getInt(ageGroup), rs.getInt("minimum_weight")};
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<BMI> loadBMITable() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
        try {
            List<BMI> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM bmi");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                BMI bmi = new BMI();
                bmi.setHeight(rs.getInt("height"));
                bmi.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                bmi.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                bmi.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                bmi.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                bmi.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                bmi.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                bmi.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                bmi.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                bmi.setMinWeight(rs.getInt("minimum_weight"));
                list.add(bmi);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<PushUp> loadPushUpTable() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
        try {
            List<PushUp> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM push_up");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                PushUp p = new PushUp();
                p.setRepetition(rs.getInt("repetiotion"));
                p.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                p.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                p.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                p.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                p.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                p.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                p.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                p.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                p.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                p.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                p.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                p.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                p.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                p.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                p.setMaleAgeGroupEight(rs.getInt("male_agegroup_eight"));
                p.setFemaleAgeGroupEight(rs.getInt("female_agegroup_eight"));
                p.setMaleAgeGroupNine(rs.getInt("male_agegroup_nine"));
                p.setFemaleAgeGroupNine(rs.getInt("female_agegroup_nine"));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<PullUp> loadPullUpTable() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
        try {
            List<PullUp> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM pull_up");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                PullUp p = new PullUp();
                p.setRepetition(rs.getInt("repetition"));
                p.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                p.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                p.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                p.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                p.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                p.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                p.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                p.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                p.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                p.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                p.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                p.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                p.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                p.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                p.setMaleAgeGroupEight(rs.getInt("male_agegroup_eight"));
                p.setFemaleAgeGroupEight(rs.getInt("female_agegroup_eight"));
                p.setMaleAgeGroupNine(rs.getInt("male_agegroup_nine"));
                p.setFemaleAgeGroupNine(rs.getInt("female_agegroup_nine"));
                list.add(p);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<BentArmHang> loadBentArmHangTable() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<BentArmHang> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM bent_arm_hang");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                BentArmHang h = new BentArmHang();
                h.setTime(rs.getInt("time"));
                h.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                h.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                h.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                h.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                h.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                h.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                h.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                h.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                h.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                h.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                list.add(h);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TrunkFormOfMovement> List<T> loadTrunkFormOfMovementTable(int id) throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<T> list = new ArrayList<>();
            if (id == SITUPCOMBOBOXELEMENT) {
                pstmt = conn.prepareStatement("SELECT * FROM sit_up");
            } else if (id == CRUNCHCOMBOBOXELEMENT) {
                pstmt = conn.prepareStatement("SELECT * FROM crunch");
            } else if (id == KNEERAISECOMBOBOXELEMENT) {
                pstmt = conn.prepareStatement("SELECT * FROM knee_raise");
            }
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                TrunkFormOfMovement trunk = null;
                if (id == SITUPCOMBOBOXELEMENT) {
                    trunk = new SitUp();
                } else if (id == CRUNCHCOMBOBOXELEMENT) {
                    trunk = new Crunch();
                } else if (id == KNEERAISECOMBOBOXELEMENT) {
                    trunk = new KneeRaise();
                }
                trunk.setRepetition(rs.getInt("repetition"));
                trunk.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                trunk.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                trunk.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                trunk.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                trunk.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                trunk.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                trunk.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                trunk.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                trunk.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                trunk.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                trunk.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                trunk.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                trunk.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                trunk.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                trunk.setMaleAgeGroupEight(rs.getInt("male_agegroup_eight"));
                trunk.setFemaleAgeGroupEight(rs.getInt("female_agegroup_eight"));
                trunk.setMaleAgeGroupNine(rs.getInt("male_agegroup_nine"));
                trunk.setFemaleAgeGroupNine(rs.getInt("female_agegroup_nine"));
                list.add((T) trunk);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Running3200> loadRunning3200Table() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<Running3200> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM running3200");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Running3200 r = new Running3200();
                r.setTime(rs.getInt("time"));
                r.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                r.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                r.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                r.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                r.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                r.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                r.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                r.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                r.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                r.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Running2000> loadRunning2000Table() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<Running2000> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM running2000");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Running2000 r = new Running2000();
                r.setTime(rs.getInt("time"));
                r.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                r.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                r.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                r.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                r.setMaleAgeGroupEight(rs.getInt("male_agegroup_eight"));
                r.setFemaleAgeGroupEight(rs.getInt("female_agegroup_eight"));
                r.setMaleAgeGroupNine(rs.getInt("male_agegroup_nine"));
                r.setFemaleAgeGroupNine(rs.getInt("female_agegroup_nine"));
                list.add(r);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Walk1600> loadWalk1600Table() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<Walk1600> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM walk1600");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Walk1600 w = new Walk1600();
                w.setTime(rs.getInt("time"));
                w.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                w.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                w.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                w.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                w.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                w.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                w.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                w.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                w.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                w.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                w.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                w.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                w.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                w.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                w.setMaleAgeGroupEight(rs.getInt("male_agegroup_eight"));
                w.setFemaleAgeGroupEight(rs.getInt("female_agegroup_eight"));
                w.setMaleAgeGroupNine(rs.getInt("male_agegroup_nine"));
                w.setFemaleAgeGroupNine(rs.getInt("female_agegroup_nine"));
                list.add(w);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public List<Ergometry> loadErgometry() throws PhysicalTestManagerDAOException {
    	PreparedStatement pstmt = null;
    	try {
            List<Ergometry> list = new ArrayList<>();
            pstmt = conn.prepareStatement("SELECT * FROM ergometry");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                Ergometry e = new Ergometry();
                e.setWattKg(rs.getInt("wattkg"));
                e.setMaleAgeGroupOne(rs.getInt("male_agegroup_one"));
                e.setFemaleAgeGroupOne(rs.getInt("female_agegroup_one"));
                e.setMaleAgeGroupTwo(rs.getInt("male_agegroup_two"));
                e.setFemaleAgeGroupTwo(rs.getInt("female_agegroup_two"));
                e.setMaleAgeGroupThree(rs.getInt("male_agegroup_three"));
                e.setFemaleAgeGroupThree(rs.getInt("female_agegroup_three"));
                e.setMaleAgeGroupFour(rs.getInt("male_agegroup_four"));
                e.setFemaleAgeGroupFour(rs.getInt("female_agegroup_four"));
                e.setMaleAgeGroupFive(rs.getInt("male_agegroup_five"));
                e.setFemaleAgeGroupFive(rs.getInt("female_agegroup_five"));
                e.setMaleAgeGroupSix(rs.getInt("male_agegroup_six"));
                e.setFemaleAgeGroupSix(rs.getInt("female_agegroup_six"));
                e.setMaleAgeGroupSeven(rs.getInt("male_agegroup_seven"));
                e.setFemaleAgeGroupSeven(rs.getInt("female_agegroup_seven"));
                list.add(e);
            }
            return list;
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public void insertBMIObject(BMI b) throws PhysicalTestManagerDAOException {
    	Statement insertBMI = null;
    	try {
            insertBMI = conn.createStatement();
            StringBuilder columnValues = new StringBuilder("(");
            columnValues.append(b.getHeight()).append(" ,").append(b.getMaleAgeGroupOne()).append(" ,").append(b.getFemaleAgeGroupOne()).append(" ,")
                    .append(b.getMaleAgeGroupTwo()).append(" ,").append(b.getFemaleAgeGroupTwo()).append(" ,").append(b.getMaleAgeGroupThree())
                    .append(" ,").append(b.getFemaleAgeGroupThree()).append(" ,").append(b.getMaleAgeGroupFour()).append(" ,").append(b.getFemaleAgeGroupFour())
                    .append(" ,").append(b.getMinWeight()).append(")");
            String sql = "INSERT INTO bmi (height, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two, male_agegroup_three,"
                    + " female_agegroup_three, male_agegroup_four, female_agegroup_four, minimum_weight) VALUES " + columnValues;
            insertBMI.executeUpdate(sql);
        } catch (Exception ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends ArmFormOfMovement> void insertArmObject(T arm) throws PhysicalTestManagerDAOException {
    	Statement insertArm = null;
    	try {
            String tableName = null;
            StringBuilder columnValues = new StringBuilder("(");
            insertArm = conn.createStatement();
            if (arm instanceof BentArmHang) {
                tableName = "bent_arm_hang";
                columnValues.append(String.valueOf(((BentArmHang) arm).getTime())).append(" ,");
                columnValues.append(arm.getMaleAgeGroupOne()).append(" ,").append(arm.getFemaleAgeGroupOne()).append(" ,").append(arm.getMaleAgeGroupTwo())
                        .append(" ,").append(arm.getFemaleAgeGroupTwo()).append(" ,").append(arm.getMaleAgeGroupThree()).append(" ,")
                        .append(arm.getFemaleAgeGroupThree()).append(" ,").append(arm.getMaleAgeGroupFour()).append(" ,").append(arm.getFemaleAgeGroupFour())
                        .append(" ,").append(arm.getMaleAgeGroupFive()).append(" ,").append(arm.getFemaleAgeGroupFive()).append(")");
                String sql = "INSERT INTO " + tableName + " (time, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup__two, "
                        + "male_agegroup_three, female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five, female_agegroup_five) VALUES " + columnValues;
                insertArm.executeUpdate(sql);
                return;
            }
            if (arm instanceof PushUp) {
                tableName = "push_up";
                columnValues.append(String.valueOf(((PushUp) arm).getRepetition())).append(" ,");
            }
            if (arm instanceof PullUp) {
                tableName = "pull_up";
                columnValues.append(String.valueOf(((PullUp) arm).getRepetition())).append(" ,");
            }
            columnValues.append(arm.getMaleAgeGroupOne()).append(" ,").append(arm.getFemaleAgeGroupOne()).append(" ,").append(arm.getMaleAgeGroupTwo())
                    .append(" ,").append(arm.getFemaleAgeGroupTwo()).append(" ,").append(arm.getMaleAgeGroupThree()).append(" ,")
                    .append(arm.getFemaleAgeGroupThree()).append(" ,").append(arm.getMaleAgeGroupFour()).append(" ,").append(arm.getFemaleAgeGroupFour())
                    .append(" ,").append(arm.getMaleAgeGroupFive()).append(" ,").append(arm.getFemaleAgeGroupFive()).append(" ,").append(arm.getMaleAgeGroupSix())
                    .append(" ,").append(arm.getFemaleAgeGroupSix()).append(" ,").append(arm.getMaleAgeGroupSeven()).append(" ,").append(arm.getFemaleAgeGroupSeven())
                    .append(" ,").append(arm.getMaleAgeGroupEight()).append(" ,").append(arm.getFemaleAgeGroupEight()).append(" ,").append(arm.getMaleAgeGroupNine())
                    .append(" ,").append(arm.getFemaleAgeGroupNine()).append(")");
            String sql = "INSERT INTO " + tableName + " (repetition, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two,"
                    + " male_agegroup_three, female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five, female_agegroup_five,"
                    + " male_agegroup_six, female_agegroup_six, male_agegroup_seven, female_agegroup_seven, male_agegroup_eight, female_agegroup_eight,"
                    + " male_agegroup_nine, female_agegroup_nine) VALUES " + columnValues;
            insertArm.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends TrunkFormOfMovement> void insertTrunkObject(T trunk) throws PhysicalTestManagerDAOException {
    	Statement insertTrunk = null;
    	try {
            String tableName = "";
            StringBuilder columnValues = new StringBuilder("(");
            if (trunk instanceof SitUp) {
                tableName = "sit_up";
            }
            if (trunk instanceof Crunch) {
                tableName = "crunch";
            }
            if (trunk instanceof KneeRaise) {
                tableName = "knee_raise";
            }
            insertTrunk = conn.createStatement();
            columnValues.append(trunk.getRepetition()).append(" ,").append(trunk.getMaleAgeGroupOne()).append(" ,").append(trunk.getFemaleAgeGroupOne()).append(" ,")
                    .append(trunk.getMaleAgeGroupTwo()).append(" ,").append(trunk.getFemaleAgeGroupTwo()).append(" ,").append(trunk.getMaleAgeGroupThree()).append(" ,")
                    .append(trunk.getFemaleAgeGroupThree()).append(" ,").append(trunk.getMaleAgeGroupFour()).append(" ,").append(trunk.getFemaleAgeGroupFour())
                    .append(" ,").append(trunk.getMaleAgeGroupFive()).append(" ,").append(trunk.getFemaleAgeGroupFive()).append(" ,").append(trunk.getMaleAgeGroupSix())
                    .append(" ,").append(trunk.getFemaleAgeGroupSix()).append(" ,").append(trunk.getMaleAgeGroupSeven()).append(" ,").append(trunk.getFemaleAgeGroupSeven())
                    .append(" ,").append(trunk.getMaleAgeGroupEight()).append(" ,").append(trunk.getFemaleAgeGroupEight()).append(" ,").append(trunk.getMaleAgeGroupNine())
                    .append(" ,").append(trunk.getFemaleAgeGroupNine()).append(")");
            String sql = "INSERT INTO " + tableName + " (repetition, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two,"
                    + " male_agegroup_three, female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five, female_agegroup_five,"
                    + " male_agegroup_six, female_agegroup_six, male_agegroup_seven, female_agegroup_seven, male_agegroup_eight, female_agegroup_eight,"
                    + " male_agegroup_nine, female_agegroup_nine) VALUES " + columnValues;
            insertTrunk.executeUpdate(sql);
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }

    @Override
    public <T extends CirculationFormOfMovement> void insertCirculationObject(T circulation) throws PhysicalTestManagerDAOException {
    	Statement insertCirculation = null;
    	try {
            String tableName = "";
            StringBuilder columnValues = new StringBuilder("(");
            insertCirculation = conn.createStatement();
            if (circulation instanceof Running3200) {
                tableName = "running3200";
                columnValues.append(((Running3200) circulation).getTime()).append(" ,");
                columnValues.append(circulation.getMaleAgeGroupOne()).append(" ,").append(circulation.getFemaleAgeGroupOne()).append(" ,").append(circulation.getMaleAgeGroupTwo())
                        .append(" ,").append(circulation.getFemaleAgeGroupTwo()).append(" ,").append(circulation.getMaleAgeGroupThree()).append(" ,").append(circulation.getFemaleAgeGroupThree())
                        .append(" ,").append(circulation.getMaleAgeGroupFour()).append(" ,").append(circulation.getFemaleAgeGroupFour()).append(" ,").append(circulation.getMaleAgeGroupFive())
                        .append(" ,").append(circulation.getFemaleAgeGroupFive()).append(")");
                String sql = "INSERT INTO " + tableName + " (time, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two, "
                        + "male_agegroup_three, female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five, female_agegroup_five) VALUES " + columnValues;
                insertCirculation.executeUpdate(sql);
                return;
            }
            if (circulation instanceof Running2000) {
                tableName = "running2000";
                columnValues.append(((Running2000) circulation).getTime()).append(" ,");
                columnValues.append(circulation.getMaleAgeGroupSix()).append(" ,").append(circulation.getFemaleAgeGroupSix()).append(" ,").append(circulation.getMaleAgeGroupSeven())
                        .append(" ,").append(circulation.getFemaleAgeGroupSeven()).append(" ,").append(circulation.getMaleAgeGroupEight()).append(" ,").append(circulation.getFemaleAgeGroupEight())
                        .append(" ,").append(circulation.getMaleAgeGroupNine()).append(" ,").append(circulation.getFemaleAgeGroupNine()).append(")");
                String sql = "INSERT INTO " + tableName + " (time, male_agegroup_six, female_agegroup_six, male_agegroup_seven, female_agegroup_seven, male_agegroup_eight, female_agegroup_eight, "
                        + "male_agegroup_nine, female_agegroup_nine) VALUES " + columnValues;
                insertCirculation.executeUpdate(sql);
                return;
            }
            if (circulation instanceof Walk1600) {
                tableName = "walk1600";
                columnValues.append(((Walk1600) circulation).getTime()).append(" ,");
                columnValues.append(circulation.getMaleAgeGroupOne()).append(" ,").append(circulation.getFemaleAgeGroupOne()).append(" ,").append(circulation.getMaleAgeGroupOne())
                        .append(" ,").append(circulation.getFemaleAgeGroupTwo()).append(" ,").append(circulation.getMaleAgeGroupThree()).append(" ,").append(circulation.getFemaleAgeGroupThree())
                        .append(" ,").append(circulation.getMaleAgeGroupFour()).append(" ,").append(circulation.getFemaleAgeGroupFour()).append(" ,").append(circulation.getMaleAgeGroupFive())
                        .append(" ,").append(circulation.getFemaleAgeGroupFive()).append(" ,").append(circulation.getMaleAgeGroupSix()).append(" ,").append(circulation.getFemaleAgeGroupSix())
                        .append(" ,").append(circulation.getMaleAgeGroupSeven()).append(" ,").append(circulation.getFemaleAgeGroupSeven()).append(" ,").append(circulation.getMaleAgeGroupEight())
                        .append(" ,").append(circulation.getFemaleAgeGroupEight()).append(" ,").append(circulation.getMaleAgeGroupNine()).append(" ,").append(circulation.getFemaleAgeGroupNine()).append(")");
                String sql = "INSERT INTO " + tableName + " (time, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two,"
                        + " male_agegroup_three, female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five,"
                        + " female_agegroup_five, male_agegroup_six, female_agegroup_six, male_agegroup_seven, female_agegroup_seven,"
                        + " male_agegroup_eight, female_agegroup_eight, male_agegroup_nine, female_agegroup_nine) VALUES " + columnValues;
                insertCirculation.executeUpdate(sql);
                return;
            }
            if (circulation instanceof Ergometry) {
                tableName = "ergometry";
                columnValues.append(((Ergometry) circulation).getWattKg()).append(" ,");
                columnValues.append(circulation.getMaleAgeGroupOne()).append(" ,").append(circulation.getFemaleAgeGroupOne()).append(" ,").append(circulation.getMaleAgeGroupTwo())
                        .append(" ,").append(circulation.getFemaleAgeGroupTwo()).append(" ,").append(circulation.getMaleAgeGroupThree()).append(" ,")
                        .append(circulation.getFemaleAgeGroupThree()).append(" ,").append(circulation.getMaleAgeGroupFour()).append(" ,").append(circulation.getFemaleAgeGroupFour())
                        .append(" ,").append(circulation.getMaleAgeGroupFive()).append(" ,").append(circulation.getFemaleAgeGroupFive()).append(" ,").append(circulation.getMaleAgeGroupSix())
                        .append(" ,").append(circulation.getFemaleAgeGroupSix()).append(" ,").append(circulation.getMaleAgeGroupSeven()).append(" ,").append(circulation.getFemaleAgeGroupSeven())
                        .append(")");
                String sql = "INSERT INTO " + tableName + " (wattkg, male_agegroup_one, female_agegroup_one, male_agegroup_two, female_agegroup_two, male_agegroup_three,"
                        + " female_agegroup_three, male_agegroup_four, female_agegroup_four, male_agegroup_five, female_agegroup_five, male_agegroup_six, female_agegroup_six,"
                        + " male_agegroup_seven, female_agegroup_seven) VALUES " + columnValues;
                insertCirculation.executeUpdate(sql);
            }
        } catch (SQLException ex) {
            throw new PhysicalTestManagerDAOException(ex.getMessage());
        }
    }
}
