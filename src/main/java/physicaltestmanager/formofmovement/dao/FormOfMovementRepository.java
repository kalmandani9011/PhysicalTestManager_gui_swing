package physicaltestmanager.formofmovement.dao;

import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.ArmFormOfMovement;
import physicaltestmanager.model.BMI;
import physicaltestmanager.model.BentArmHang;
import physicaltestmanager.model.CirculationFormOfMovement;
import physicaltestmanager.model.Ergometry;
import physicaltestmanager.model.PullUp;
import physicaltestmanager.model.PushUp;
import physicaltestmanager.model.Running2000;
import physicaltestmanager.model.Running3200;
import physicaltestmanager.model.TrunkFormOfMovement;
import physicaltestmanager.model.Walk1600;

/**
 * Az alapadatokat tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface FormOfMovementRepository {
    
    /**
     * Kikeresi a pontszámot az adott táblából.
     * @param table A tábla neve, amelyikből a pontszámot kell kikeresni.
     * @param result Ami alapján a sort azonosítjuk be.
     * @param ageGroup Ami alapján az oszlopot azonosítjuk be.
     * @return A megadott cella (pontszám) értékét adja vissza.
     */
    int findScore(String table, int result, String ageGroup) throws PhysicalTestManagerDAOException;
    
    /**
     * Kikeresi a BMI értéket a táblából.
     * @param height Ami alapján a sort azonosítjuk be.
     * @param ageGroup Ami alapján az oszlopot azonosítjuk be.
     * @return Egy tömböt ad vissza, melynek első eleme az adott cella értéke (ez a maximális engedélyezett súly),
     * a második eleme pedig a minimum súly. E két értéken belül megfelelő az adott személy BMI indexe.
     */
    int[] findBMIScore(int height, String ageGroup) throws PhysicalTestManagerDAOException;
    
    /**
     * A BMI táblázat listába való beolvasása.
     */
    List<BMI> loadBMITable() throws PhysicalTestManagerDAOException;
    
    /**
     * A fekvő táblázat listába való beolvasása.
     */
    List<PushUp> loadPushUpTable() throws PhysicalTestManagerDAOException;
    
    /**
     * A húzódzkodás táblázat listába való beolvasása.
     */
    List<PullUp> loadPullUpTable() throws PhysicalTestManagerDAOException;
    
    /**
     * A hajlított függés táblázat listába való beolvasása.
     */
    List<BentArmHang> loadBentArmHangTable() throws PhysicalTestManagerDAOException;
    
    /**
     * A törzs mozgásformák (felülés, lapocka emelés, függő térdemelés)
     * táblázatai listába való beolvasása.
     */
    <T extends TrunkFormOfMovement> List<T> loadTrunkFormOfMovementTable(int id) throws PhysicalTestManagerDAOException;
    
    /**
     * A 3200m futás táblázat listába való beolvasása.
     */
    List<Running3200> loadRunning3200Table() throws PhysicalTestManagerDAOException;
    
    /**
     * A 2000m futás táblázat listába való beolvasása.
     */
    List<Running2000> loadRunning2000Table() throws PhysicalTestManagerDAOException;
    
    /**
     * Az 1600m menet táblázat listába való beolvasása.
     */
    List<Walk1600> loadWalk1600Table() throws PhysicalTestManagerDAOException;
    
    /**
     * Az ergometria táblázat listába való beolvasása.
     */
    List<Ergometry> loadErgometry() throws PhysicalTestManagerDAOException;
    
    /**
     * BMI objektum adatbázisba való hozzáadása.
     */
    void insertBMIObject(BMI b) throws PhysicalTestManagerDAOException;
    
    /**
     * Fekvőtámasz, húzódzkodás, függő térdemelés objektum adatbázisba való
     * hozzáadása.
     */
    <T extends ArmFormOfMovement> void insertArmObject(T arm) throws PhysicalTestManagerDAOException;
    
    /**
     * Felülés, lapocka emelés, függő térdemelés objektum adatbázisba való
     * hozzáadása.
     */
    <T extends TrunkFormOfMovement> void insertTrunkObject(T trunk) throws PhysicalTestManagerDAOException;
    
    /**
     * 3200 m, 2000 m futás, 1600 m menet, ergometria objektum adatbázisba való
     * hozzáadása.
     */
    <T extends CirculationFormOfMovement> void insertCirculationObject(T circulation) throws PhysicalTestManagerDAOException;
}
