package physicaltestmanager.comparator;

import java.util.Comparator;

import physicaltestmanager.model.Person;

/**
 *
 * @author
 */
public class NameComparator implements Comparator<Person> {
    
    @Override
    public int compare(Person sz, Person sz2) {
        return sz.getName().compareToIgnoreCase(sz2.getName());
    }
}
