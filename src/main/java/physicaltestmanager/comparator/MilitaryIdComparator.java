package physicaltestmanager.comparator;

import java.util.Comparator;

import physicaltestmanager.model.Person;

/**
 *
 * @author
 */
public class MilitaryIdComparator implements Comparator<Person> {
    
    @Override
    public int compare(Person sz, Person sz2) {
        return sz.getMilitaryId() - sz2.getMilitaryId();
    }
}
