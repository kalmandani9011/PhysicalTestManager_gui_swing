package physicaltestmanager.comparator;

import java.util.Comparator;

import physicaltestmanager.model.Person;

/**
 *
 * @author
 */
public class IdComparator implements Comparator<Person> {
    
    @Override
    public int compare(Person sz, Person sz2) {
        return sz.getId().compareTo(sz2.getId());
    }
}
