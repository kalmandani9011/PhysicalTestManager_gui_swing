package physicaltestmanager.gui;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import physicaltestmanager.model.Person;
import physicaltestmanager.model.PhysicalTest;

/**
 * Új fizikai állapot felméréshez vagy meglévő módosításához használható dialógus ablak.
 */
public class PhysicalTestDialog extends javax.swing.JDialog {

    private static int RUNNINGAGELIMIT = 45;
    private static int ERGOMETRYAGELIMIT = 54;
    private static final int FIRSTCOMBOBOXELEMENT = 0;
    private static final int SECONDCOMBOBOXELEMENT = RUNNING2000COMBOBOXELEMENT = 1;
    private static final int THIRDCOMBOBOXELEMENT = 2;
    private static int RUNNING2000COMBOBOXELEMENT;
    private static int ERGOMETRYCOMBOBOXELEMENT = 3;
    private static int MAXMINUTE = 59;
    private Person person;
    private Optional<PhysicalTest> optTest;

    public PhysicalTestDialog(java.awt.Frame parent, Person person) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Új fizikai eredmény hozzáadása");
        setLocationRelativeTo(null);
        this.person = person;
        optTest = Optional.empty();
        tfMilitaryId.setText(person.getMilitaryId() + "");
        tfMilitaryId.setEnabled(false);
        tfArm.setForeground(Color.LIGHT_GRAY);
        tfTrunk.setForeground(Color.LIGHT_GRAY);
        tfCirculation.setForeground(Color.LIGHT_GRAY);
    }
    
    public PhysicalTestDialog(java.awt.Frame parent, Person person, Optional<PhysicalTest> optTest) {
        super(parent);
        initComponents();
        this.setSize(570, 480);
        this.setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Fizikai eredmény módosítása");
        this.setLocationRelativeTo(null);
        this.person = person;
        if (optTest.isPresent()) {
            this.optTest = optTest;
            tfMilitaryId.setText(person.getMilitaryId() + "");
            tfMilitaryId.setEnabled(false);
            tfDateOfExecution.setText(optTest.get().getDateOfExecution().toString());
            cbArm.setSelectedIndex(optTest.get().getArmFormOfMovement());
            tfArm.setText(optTest.get().getArmResult() + "");
            cbTrunk.setSelectedIndex(optTest.get().getTrunkFormOfMovement());
            tfTrunk.setText(optTest.get().getTrunkResult() + "");
            cbCirculation.setSelectedIndex(optTest.get().getCirculationFormOfMovement());
            tfCirculation.setText(optTest.get().getCirculationResult() + "");
        }
    }
    
    public Optional<PhysicalTest> showDialog() {
        setVisible(true);
        return optTest;
    }

    private boolean fieldCheck() {
        try {
            LocalDate.parse(tfDateOfExecution.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        } catch (DateTimeParseException ex) {
            popUpWindow("Hibás végrehajtási idő!");
            return false;
        }
        if (tfArm.getText().isEmpty() || !tfArm.getText().matches("[0-9]+") || Integer.parseInt(tfArm.getText()) < 0 || Integer.parseInt(tfArm.getText()) > 150) {
            popUpWindow("Hiba a Kar-vállöv erő-állóképesség bevitelnél!");
            return false;
        } else if (tfTrunk.getText().isEmpty() || !tfTrunk.getText().matches("[0-9]+") || Integer.parseInt(tfTrunk.getText()) < 0 || Integer.parseInt(tfTrunk.getText()) > 150) {
            popUpWindow("Hiba a Törzs erő-állóképesség bevitelnél!");
            return false;
        } else if (tfCirculation.getText().isEmpty() || !tfCirculation.getText().matches("[0-9]+")) {
            popUpWindow("Hiba a Keringésrendszer állóképesség bevitelnél!");
            return false;
        } else if (cbCirculation.getSelectedIndex() == ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) > 600
                || cbCirculation.getSelectedIndex() == ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) < 40) {
            popUpWindow("Nem érvényes watt/kg formátum!");
            return false;
        } else if (cbCirculation.getSelectedIndex() != ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) > 1000 && Integer.parseInt(tfCirculation.getText().substring(2)) > MAXMINUTE) {
            popUpWindow("Nem érvényes idő formátum!");
            return false;
        } else if (cbCirculation.getSelectedIndex() != ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) < 1000 && Integer.parseInt(tfCirculation.getText().substring(1)) > MAXMINUTE) {
            popUpWindow("Nem érvényes idő formátum!");
            return false;
        } else if (cbCirculation.getSelectedIndex() != ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) < 400
                || cbCirculation.getSelectedIndex() != ERGOMETRYCOMBOBOXELEMENT && Integer.parseInt(tfCirculation.getText()) > 2800) {
            popUpWindow("Nem érvényes idő formátum!");
            return false;
        } else if (cbCirculation.getSelectedIndex() == RUNNING2000COMBOBOXELEMENT && person.getAge() < RUNNINGAGELIMIT) {
            popUpWindow("45 év alatt nem lehet 2000 m-t futni!");
            return false;
        } else if (cbCirculation.getSelectedIndex() == ERGOMETRYCOMBOBOXELEMENT && person.getAge() > ERGOMETRYAGELIMIT) {
            popUpWindow("54 év felett nem lehet ergometriát alkalmazni!");
            return false;
        } else {
            return true;
        }
    }

    private int popUpWindow(String errorMessage) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(errorMessage));
        return JOptionPane.showConfirmDialog(null, panel, "Hiba!", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
    }

    private void add() {
        LocalDate dateOfExecution = LocalDate.parse(tfDateOfExecution.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        int armFormOfMovement = cbArm.getSelectedIndex();
        int armResult = Integer.parseInt(tfArm.getText());
        int trunkFormOfMovement = cbTrunk.getSelectedIndex();
        int trunkResult = Integer.parseInt(tfTrunk.getText());
        int circulationFormOfMovement = cbCirculation.getSelectedIndex();
        int circulationResult = Integer.parseInt(tfCirculation.getText());
        if (optTest.isPresent()) {
            optTest = Optional.of(new PhysicalTest(optTest.get().getId(), person.getMilitaryId(), dateOfExecution, armFormOfMovement, armResult, 
            trunkFormOfMovement, trunkResult, circulationFormOfMovement, circulationResult));
        } else {
            optTest = Optional.of(new PhysicalTest(person.getMilitaryId(), dateOfExecution, armFormOfMovement, armResult, 
            trunkFormOfMovement, trunkResult, circulationFormOfMovement, circulationResult));
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLblMilitaryId = new javax.swing.JLabel();
        tfMilitaryId = new javax.swing.JTextField();
        jLblDateOfExecution = new javax.swing.JLabel();
        tfDateOfExecution = new javax.swing.JTextField();
        lblArm = new javax.swing.JLabel();
        cbArm = new javax.swing.JComboBox<>();
        tfArm = new javax.swing.JTextField();
        lblTrunk = new javax.swing.JLabel();
        cbTrunk = new javax.swing.JComboBox<>();
        tfTrunk = new javax.swing.JTextField();
        lblCirculation = new javax.swing.JLabel();
        cbCirculation = new javax.swing.JComboBox<>();
        tfCirculation = new javax.swing.JTextField();
        jbOk = new javax.swing.JButton();
        jbCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLblMilitaryId.setText("SZTSZ:");

        jLblDateOfExecution.setText("Végrehajtás ideje:");

        tfDateOfExecution.setToolTipText("Elválasztó lehet: pont, kötőjel vagy semmi.");
        tfDateOfExecution.setText("1900.01.01.");
        tfDateOfExecution.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfDateOfExecutionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfDateOfExecutionFocusLost(evt);
            }
        });

        lblArm.setText("Kar-vállöv erő-állóképesség");

        cbArm.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Fekvőtámasz", "Húzódzkodás", "Hajlított karú függés" }));
        cbArm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbArmItemStateChanged(evt);
            }
        });

        tfArm.setText("0..75");
        tfArm.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfArmFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfArmFocusLost(evt);
            }
        });

        lblTrunk.setText("Törzs erő-állóképesség");

        cbTrunk.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Felülés", "Lapocka emelés", "Függő térdemelés" }));
        cbTrunk.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTrunkItemStateChanged(evt);
            }
        });

        tfTrunk.setText("0..90");
        tfTrunk.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfTrunkFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfTrunkFocusLost(evt);
            }
        });

        lblCirculation.setText("Keringésrendszer állóképesség");

        cbCirculation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "3200m futás", "2000m futás", "1600m menet", "Ergometria" }));

        tfCirculation.setToolTipText("Az időt elválasztás nélkül kell beírni! Pl.: 1234");
        tfCirculation.setText("400..2800");
        tfCirculation.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfCirculationFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfCirculationFocusLost(evt);
            }
        });

        jbOk.setText("OK");
        jbOk.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbOk.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbOkActionPerformed(e);
            }
        });
        jbOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOkActionPerformed(evt);
            }
        });

        jbCancel.setText("Mégsem");
        jbCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblCirculation)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbCirculation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(51, 51, 51)
                                .addComponent(tfCirculation, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLblDateOfExecution)
                                    .addComponent(jLblMilitaryId))
                                .addGap(53, 53, 53)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(tfDateOfExecution, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(tfMilitaryId, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(lblTrunk)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lblArm)
                                        .addGap(141, 141, 141))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cbArm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(tfArm))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(cbTrunk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(tfTrunk, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(62, 62, 62)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jbCancel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jbOk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLblMilitaryId)
                    .addComponent(tfMilitaryId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfDateOfExecution, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLblDateOfExecution))
                .addGap(18, 18, 18)
                .addComponent(lblArm)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbArm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfArm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbOk))
                .addGap(18, 18, 18)
                .addComponent(lblTrunk)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbTrunk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfTrunk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbCancel))
                .addGap(18, 18, 18)
                .addComponent(lblCirculation)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbCirculation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfCirculation, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(65, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOkActionPerformed
        if (fieldCheck()) {
            add();
            setVisible(false);
            dispose();
        } else {
            optTest = Optional.empty();
        }
    }//GEN-LAST:event_jbOkActionPerformed

    private void jbCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMegsemActionPerformed
        optTest = Optional.empty();
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbMegsemActionPerformed

    private void tfDateOfExecutionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfVegrehajtasIdejeFocusGained
        tfDateOfExecution.setForeground(Color.BLACK);
        if (tfDateOfExecution.getText().trim().equals("1900.01.01.")) {
            tfDateOfExecution.setText("");
        }
    }//GEN-LAST:event_tfVegrehajtasIdejeFocusGained

    private void tfDateOfExecutionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfVegrehajtasIdejeFocusLost
        if (tfDateOfExecution.getText().trim().isEmpty()) {
            tfDateOfExecution.setForeground(Color.LIGHT_GRAY);
            tfDateOfExecution.setText("1900.01.01.");
        }
    }//GEN-LAST:event_tfVegrehajtasIdejeFocusLost

    private void cbArmItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbKarItemStateChanged
        if (tfArm.getText().trim().equals("0..75") || tfArm.getText().trim().equals("0..20") || tfArm.getText().trim().equals("0..70")) {
            tfArm.setForeground(Color.LIGHT_GRAY);
            if (cbArm.getSelectedIndex() == FIRSTCOMBOBOXELEMENT) {
                tfArm.setText("0..75");
            } else if (cbArm.getSelectedIndex() == SECONDCOMBOBOXELEMENT) {
                tfArm.setText("0..20");
            } else if (cbArm.getSelectedIndex() == THIRDCOMBOBOXELEMENT) {
                tfArm.setText("0..70");
            }
        }
    }//GEN-LAST:event_cbKarItemStateChanged

    private void tfArmFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKarFocusGained
        tfArm.setForeground(Color.BLACK);
        if (tfArm.getText().trim().equals("0..75") || tfArm.getText().trim().equals("0..20") || tfArm.getText().trim().equals("0..70")) {
            tfArm.setText("");
        }
    }//GEN-LAST:event_tfKarFocusGained

    private void tfArmFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKarFocusLost
        if (tfArm.getText().trim().isEmpty()) {
            tfArm.setForeground(Color.LIGHT_GRAY);
            if (cbArm.getSelectedIndex() == FIRSTCOMBOBOXELEMENT) {
                tfArm.setText("0..75");
            } else if (cbArm.getSelectedIndex() == SECONDCOMBOBOXELEMENT) {
                tfArm.setText("0..20");
            } else if (cbArm.getSelectedIndex() == THIRDCOMBOBOXELEMENT) {
                tfArm.setText("0..70");
            }
        }
    }//GEN-LAST:event_tfKarFocusLost

    private void cbTrunkItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTorzsItemStateChanged
        if (tfTrunk.getText().trim().equals("0..90") || tfTrunk.getText().trim().equals("0..72") || tfTrunk.getText().trim().equals("0..51")) {
            tfTrunk.setForeground(Color.LIGHT_GRAY);
            if (cbTrunk.getSelectedIndex() == FIRSTCOMBOBOXELEMENT) {
                tfTrunk.setText("0..90");
            } else if (cbTrunk.getSelectedIndex() == SECONDCOMBOBOXELEMENT) {
                tfTrunk.setText("0..72");
            } else if (cbTrunk.getSelectedIndex() == THIRDCOMBOBOXELEMENT) {
                tfTrunk.setText("0..51");
            }
        }
    }//GEN-LAST:event_cbTorzsItemStateChanged

    private void tfTrunkFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTorzsFocusGained
        tfTrunk.setForeground(Color.BLACK);
        if (tfTrunk.getText().trim().equals("0..90") || tfTrunk.getText().trim().equals("0..72") || tfTrunk.getText().trim().equals("0..51")) {
            tfTrunk.setText("");
        }
    }//GEN-LAST:event_tfTorzsFocusGained

    private void tfTrunkFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTorzsFocusLost
        if (tfTrunk.getText().trim().isEmpty()) {
            tfTrunk.setForeground(Color.LIGHT_GRAY);
            if (cbTrunk.getSelectedIndex() == FIRSTCOMBOBOXELEMENT) {
                tfTrunk.setText("0..90");
            } else if (cbTrunk.getSelectedIndex() == SECONDCOMBOBOXELEMENT) {
                tfTrunk.setText("0..72");
            } else if (cbTrunk.getSelectedIndex() == THIRDCOMBOBOXELEMENT) {
                tfTrunk.setText("0..51");
            }
        }
    }//GEN-LAST:event_tfTorzsFocusLost
    
    private void tfCirculationFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKeringesFocusGained
        tfCirculation.setForeground(Color.BLACK);
        if (tfCirculation.getText().trim().equals("400..2800")) {
            tfCirculation.setText("");
        }
    }//GEN-LAST:event_tfKeringesFocusGained

    private void tfCirculationFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfKeringesFocusLost
        if (tfCirculation.getText().trim().isEmpty()) {
            tfCirculation.setForeground(Color.LIGHT_GRAY);
            tfCirculation.setText("400..2800");
        }
    }//GEN-LAST:event_tfKeringesFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbArm;
    private javax.swing.JComboBox<String> cbCirculation;
    private javax.swing.JComboBox<String> cbTrunk;
    private javax.swing.JLabel jLblMilitaryId;
    private javax.swing.JLabel jLblDateOfExecution;
    private javax.swing.JButton jbCancel;
    private javax.swing.JButton jbOk;
    private javax.swing.JLabel lblArm;
    private javax.swing.JLabel lblCirculation;
    private javax.swing.JLabel lblTrunk;
    private javax.swing.JTextField tfArm;
    private javax.swing.JTextField tfCirculation;
    private javax.swing.JTextField tfMilitaryId;
    private javax.swing.JTextField tfTrunk;
    private javax.swing.JTextField tfDateOfExecution;
    // End of variables declaration//GEN-END:variables
}
