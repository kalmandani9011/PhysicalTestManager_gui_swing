package physicaltestmanager.gui;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;

import java.awt.event.ActionEvent;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import physicaltestmanager.controller.GuiController;

/**
 *
 * @author
 */
public class SearchDialog extends javax.swing.JDialog {

    private ButtonGroup group;
    private GuiController controller;
    private Optional<String> optSearch;

    /**
     * Creates new form SearchDialog
     */
    public SearchDialog(java.awt.Frame parent, GuiController controller) {
        super(parent);
        initComponents();
        setLocationRelativeTo(null);
        setModal(true);
        setTitle("Keresés");
        this.controller = controller;
        group = new ButtonGroup();
        group.add(jrbName);
        group.add(jrbMilitaryId);
        group.setSelected(jrbName.getModel(), true);
    }

    public String getInput() {
        return tfInput.getText();
    }
    
    public Optional<String> showDialog() {
        setVisible(true);
        return optSearch;
    }

    public boolean fieldCheck() {
        String regex = "[\\p{javaUpperCase}][\\w.]*[ -][\\p{javaUpperCase}][\\w.]*( [\\p{javaUpperCase}][\\w]*)?( [\\p{javaUpperCase}][\\w]*)?";
        Pattern p = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher m = p.matcher(tfInput.getText());
        if (group.isSelected(jrbName.getModel())) {
            if (tfInput.getText().isEmpty() || !m.matches()) {
                popUpWindow("Hiba a név bevitelnél!");
                return false;
            }
        } else if (group.isSelected(jrbMilitaryId.getModel())) {
            if (tfInput.getText().isEmpty() || !tfInput.getText().matches("\\b([0-9]{8})\\b")) {
                popUpWindow("Hiba az SZTSZ bevitelnél!");
                return false;
            }
        }
        return true;
    }

    private int popUpWindow(String errorMessage) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(errorMessage));
        return JOptionPane.showConfirmDialog(null, panel, "Hiba!", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tfInput = new javax.swing.JTextField();
        jrbName = new javax.swing.JRadioButton();
        jrbMilitaryId = new javax.swing.JRadioButton();
        jbSearch = new javax.swing.JButton();
        jbCancel = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jrbName.setText("Név szerint");

        jrbMilitaryId.setText("SZTSZ szerint");

        jbSearch.setText("Keress");
        jbSearch.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbSearch.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbSearchActionPerformed(e);
            }
        });
        jbSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSearchActionPerformed(evt);
            }
        });

        jbCancel.setText("Mégse");
        jbCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jbSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jbCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jrbName)
                        .addGap(65, 65, 65)
                        .addComponent(jrbMilitaryId))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(tfInput, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jrbName)
                    .addComponent(jrbMilitaryId))
                .addGap(40, 40, 40)
                .addComponent(tfInput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbSearch)
                    .addComponent(jbCancel))
                .addGap(27, 27, 27))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jbCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMegseActionPerformed
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbMegseActionPerformed

    private void jbSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbKeresActionPerformed
        if (fieldCheck()) {
            optSearch = Optional.of(tfInput.getText());
            setVisible(false);
            dispose();
        }
    }//GEN-LAST:event_jbKeresActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jbSearch;
    private javax.swing.JButton jbCancel;
    private javax.swing.JRadioButton jrbName;
    private javax.swing.JRadioButton jrbMilitaryId;
    private javax.swing.JTextField tfInput;
    // End of variables declaration//GEN-END:variables
}
