package physicaltestmanager.gui;

import static javax.swing.JComponent.WHEN_IN_FOCUSED_WINDOW;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import physicaltestmanager.model.Person;
import physicaltestmanager.model.Rank;

/**
 * Új személy felviteléhez, vagy meglévő módosításához használandó dialógus ablak.
 */
public class PersonDialog extends javax.swing.JDialog {

    private static int MINHEIGHT = 130;
    private static int MAXHEIGHT = 230;
    private static int MINWEIGHT = 40;
    private static int MAXWEIGHT = 170;
    private List<Integer> militaryIds;
    private Optional<Person> optPerson;
    private int rankIndex;
    private int unitIndex;
    private int sexIndex;

    public PersonDialog(java.awt.Frame parent) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setTitle("Új személy");
        optPerson = Optional.empty();
        setLocationRelativeTo(null);
        tfName.setForeground(Color.LIGHT_GRAY);
        tfMilitaryId.setForeground(Color.LIGHT_GRAY);
        tfBirthDate.setForeground(Color.LIGHT_GRAY);
        tfMothersMaidenName.setForeground(Color.LIGHT_GRAY);
        tfHeight.setForeground(Color.LIGHT_GRAY);
        tfWeight.setForeground(Color.LIGHT_GRAY);
        tfBodyFat.setForeground(Color.LIGHT_GRAY);
    }
    
    public PersonDialog(java.awt.Frame parent, Optional<Person> optPerson) {
        super(parent);
        initComponents();
        setSize(570, 480);
        setModalityType(DEFAULT_MODALITY_TYPE);
        setLocationRelativeTo(null);
        if (optPerson.isPresent()) {
            setTitle("Személy módosítása");
            this.optPerson = optPerson;
            tfName.setText(optPerson.get().getName());
            tfMilitaryId.setText(Integer.toString(optPerson.get().getMilitaryId()));
            tfBirthDate.setText(optPerson.get().getBirthDate().toString());
            tfMothersMaidenName.setText(optPerson.get().getMothersMaidenName());
            tfHeight.setText(Integer.toString(optPerson.get().getHeight()));
            tfWeight.setText(Integer.toString(optPerson.get().getWeight()));
            tfBodyFat.setText(Double.toString(optPerson.get().getBodyFat()));
            cbRank.setSelectedItem(optPerson.get().getRank());
            cbSex.setSelectedIndex(optPerson.get().isMale() ? 0 : 1);
            cbUnit.setSelectedItem(optPerson.get().getUnit());
        }
    }
    
    public Optional<Person> showDialog() {
        setVisible(true);
        return optPerson;
    }
    
    public void setMilitaryIds(List<Integer> militaryIds) {
        this.militaryIds = militaryIds;
    }
    
    private boolean fieldCheck() {
        String regex = "[\\p{javaUpperCase}][\\w.]*[ -][\\p{javaUpperCase}][\\w.]*( [\\p{javaUpperCase}][\\w]*)?( [\\p{javaUpperCase}][\\w]*)?";
        Pattern p = Pattern.compile(regex, Pattern.UNICODE_CHARACTER_CLASS);
        Matcher m = p.matcher(tfName.getText());
        if (tfName.getText().trim().isEmpty() || !m.matches()) {
            popUpWindow("Hiba a név bevitelnél!");
            return false;
        } else if (tfMilitaryId.getText().isEmpty() || !tfMilitaryId.getText().matches("\\b([0-9]{8})\\b")) {
            popUpWindow("Hiba az SZTSZ bevitelnél!");
            return false;
        }
        for (int i : militaryIds) {
            if (!optPerson.isPresent()) {
                if (i == Integer.parseInt(tfMilitaryId.getText())) {
                    popUpWindow("Már létezik személy ezzel az SZTSZ számmal!");
                    return false;
                }
            } else {
                if (optPerson.get().getMilitaryId() != Integer.parseInt(tfMilitaryId.getText()) && i == Integer.parseInt(tfMilitaryId.getText())) {
                    popUpWindow("Már létezik személy ezzel az SZTSZ számmal!");
                    return false;
                }
            }
        }
        try {
            LocalDate.parse(tfBirthDate.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        } catch (DateTimeParseException ex) {
            popUpWindow("Hibás születési dátum!");
            return false;
        }
        m = p.matcher(tfMothersMaidenName.getText().trim());
        if (tfMothersMaidenName.getText().trim().isEmpty() || !m.matches()) {
            popUpWindow("Hiba az anyja neve bevitelnél!");
            return false;
        } else if (tfHeight.getText().isEmpty() || !tfHeight.getText().matches("[0-9]+") || Integer.parseInt(tfHeight.getText()) < MINHEIGHT
                || Integer.parseInt(tfHeight.getText()) > MAXHEIGHT) {
            popUpWindow("Hiba a magasság bevitelnél!");
            return false;
        } else if (tfWeight.getText().isEmpty() || !tfWeight.getText().matches("[0-9]+") || Integer.parseInt(tfWeight.getText()) < MINWEIGHT
                || Integer.parseInt(tfWeight.getText()) > MAXWEIGHT) {
            popUpWindow("Hiba a súly bevitelnél!");
            return false;
        } else if (tfBodyFat.getText().isEmpty() || !tfBodyFat.getText().matches("[0-9]+[.,]?[0-9]?")) {
            popUpWindow("Pontot vagy vesszőt használj tizedesjelnek!");
            return false;
        } else {
            return true;
        }
    }

    private int popUpWindow(String hibaUzenet) {
        JPanel panel = new JPanel();
        panel.add(new JLabel(hibaUzenet));
        return JOptionPane.showConfirmDialog(null, panel, "Hiba!", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
    }
    
    private void add() {
        String name = tfName.getText().trim();
        int militaryId = Integer.parseInt(tfMilitaryId.getText());
        Rank rank = cbRank.getItemAt(rankIndex);
        String unit = cbUnit.getItemAt(unitIndex);
        LocalDate birthDate = LocalDate.parse(tfBirthDate.getText(), DateTimeFormatter.ofPattern("[yyyy.MM.dd.][yyyy.MM.dd][yyyy-MM-dd][yyyyMMdd]"));
        String mothersMaidenName = tfMothersMaidenName.getText().trim();
        boolean male = sexIndex == 0;
        int height = Integer.parseInt(tfHeight.getText());
        int weight = Integer.parseInt(tfWeight.getText());
        double bodyFat = Double.parseDouble(tfBodyFat.getText().replaceAll(",", "."));
        if (optPerson.isPresent()) {
            optPerson = Optional.of(new Person(optPerson.get().getId(), militaryId, name, rank, unit, birthDate, mothersMaidenName, male, height, weight, bodyFat));
        } else {
            optPerson = Optional.of(new Person(militaryId, name, rank, unit, birthDate, mothersMaidenName, male, height, weight, bodyFat));
        }
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jbOk = new javax.swing.JButton();
        jbCancel = new javax.swing.JButton();
        jLblName = new javax.swing.JLabel();
        jLblMilitaryId = new javax.swing.JLabel();
        tfName = new javax.swing.JTextField();
        tfBirthDate = new javax.swing.JTextField();
        jLblBirthDate = new javax.swing.JLabel();
        tfMilitaryId = new javax.swing.JTextField();
        jLblMothersMaidenName = new javax.swing.JLabel();
        tfMothersMaidenName = new javax.swing.JTextField();
        jLblHeight = new javax.swing.JLabel();
        tfHeight = new javax.swing.JTextField();
        jLblWeight = new javax.swing.JLabel();
        tfWeight = new javax.swing.JTextField();
        jLblBodyFat = new javax.swing.JLabel();
        tfBodyFat = new javax.swing.JTextField();
        jLblRank = new javax.swing.JLabel();
        cbRank = new javax.swing.JComboBox<>();
        jLblUnit = new javax.swing.JLabel();
        cbUnit = new javax.swing.JComboBox<>();
        cbSex = new javax.swing.JComboBox<>();
        jLblSex = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jbOk.setText("OK");
        jbOk.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("ENTER"), "OK");
        jbOk.getActionMap().put("OK", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jbOkActionPerformed(e);
            }
        });
        jbOk.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOkActionPerformed(evt);
            }
        });

        jbCancel.setText("Mégsem");
        jbCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbCancelActionPerformed(evt);
            }
        });

        jLblName.setText("Név:");

        jLblMilitaryId.setText("SZTSZ:");

        tfName.setToolTipText("Max 4 tagú név lehetséges, az első 2 tag között kötőjel is engedélyezett.");
        tfName.setText("pl.: Dr. Kiss Nagy Aladdin");
        tfName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfNameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfNameFocusLost(evt);
            }
        });

        tfBirthDate.setToolTipText("Elválasztó lehet: pont, kötőjel vagy semmi.");
        tfBirthDate.setText("1900.01.01.");
        tfBirthDate.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfBirthDateFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfBirthDateFocusLost(evt);
            }
        });

        jLblBirthDate.setText("Születési dátum:");

        tfMilitaryId.setToolTipText("8 számjegyből kell állnia!");
        tfMilitaryId.setText("12345678");
        tfMilitaryId.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfMilitaryIdFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfMilitaryIdFocusLost(evt);
            }
        });

        jLblMothersMaidenName.setText("Anyja neve:");

        tfMothersMaidenName.setToolTipText("Max 4 tagú név lehetséges, az első 2 tag között kötőjel is engedélyezett.");
        tfMothersMaidenName.setText("pl.: Dr. Kiss Nagy Aladdin");
        tfMothersMaidenName.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfMothersMaidenNameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfMothersMaidenNameFocusLost(evt);
            }
        });

        jLblHeight.setText("Magasság:");

        tfHeight.setToolTipText("Nem kell mértékegység!");
        tfHeight.setText("pl.: 175");
        tfHeight.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfHeightFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfHeightFocusLost(evt);
            }
        });

        jLblWeight.setText("Súly:");

        tfWeight.setToolTipText("Nem kell mértékegység!");
        tfWeight.setText("pl.: 75");
        tfWeight.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfWeightFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfWeightFocusLost(evt);
            }
        });

        jLblBodyFat.setText("Testzsír százalék:");

        tfBodyFat.setToolTipText("Nem kell százalékjel! Tizedesjel pont vagy vessző legyen! Egy tizedesjegyet írj!");
        tfBodyFat.setText("pl.: 10,5");
        tfBodyFat.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                tfBodyFatFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfBodyFatFocusLost(evt);
            }
        });

        jLblRank.setText("Rendfokozat:");

        cbRank.setModel(new javax.swing.DefaultComboBoxModel<>(Rank.values()));
        cbRank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbRankActionPerformed(evt);
            }
        });

        jLblUnit.setText("Alakulat:");

        cbUnit.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bocskai", "Klapka" }));
        cbUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbUnitActionPerformed(evt);
            }
        });

        cbSex.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Férfi", "Nő" }));
        cbSex.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbSexActionPerformed(evt);
            }
        });

        jLblSex.setText("Neme:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLblName, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLblMilitaryId)
                            .addComponent(jLblBirthDate)
                            .addComponent(jLblMothersMaidenName)
                            .addComponent(jLblHeight)
                            .addComponent(jLblWeight))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfMothersMaidenName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(tfMilitaryId, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(tfBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(tfHeight, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                                            .addComponent(tfWeight))
                                        .addGap(75, 75, 75)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLblRank)
                                            .addComponent(jLblUnit)
                                            .addComponent(jLblSex))
                                        .addGap(28, 28, 28)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(cbSex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cbUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(cbRank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(jbOk, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(82, 82, 82)
                                        .addComponent(jbCancel)))
                                .addContainerGap(84, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLblBodyFat)
                        .addGap(18, 18, 18)
                        .addComponent(tfBodyFat, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLblName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLblMilitaryId)
                    .addComponent(tfMilitaryId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfBirthDate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLblBirthDate))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLblMothersMaidenName)
                    .addComponent(tfMothersMaidenName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(jLblHeight))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(tfHeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tfWeight, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLblWeight))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLblBodyFat)
                            .addComponent(tfBodyFat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(36, 36, 36))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLblRank)
                            .addComponent(cbRank, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbSex, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLblSex))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLblUnit)
                            .addComponent(cbUnit))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jbOk)
                    .addComponent(jbCancel))
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbSexActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNemActionPerformed
        sexIndex = cbSex.getSelectedIndex();
    }//GEN-LAST:event_cbNemActionPerformed

    private void jbCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbMegsemActionPerformed
        optPerson = Optional.empty();
        setVisible(false);
        dispose();
    }//GEN-LAST:event_jbMegsemActionPerformed

    private void jbOkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOkActionPerformed
        if (fieldCheck()) {
            add();
            setVisible(false);
            dispose();
        } else {
            optPerson = Optional.empty();
        }
    }//GEN-LAST:event_jbOkActionPerformed

    private void cbRankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbRendfokozatActionPerformed
        rankIndex = cbRank.getSelectedIndex();
    }//GEN-LAST:event_cbRendfokozatActionPerformed

    private void cbUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAlakulatActionPerformed
        unitIndex = cbUnit.getSelectedIndex();
    }//GEN-LAST:event_cbAlakulatActionPerformed

    private void tfNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfNevFocusGained
        tfName.setForeground(Color.BLACK);
        if (tfName.getText().trim().equals("pl.: Dr. Kiss Nagy Aladdin")) {
            tfName.setText("");
        }
    }//GEN-LAST:event_tfNevFocusGained

    private void tfNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfNevFocusLost
        if (tfName.getText().trim().isEmpty()) {
            tfName.setForeground(Color.LIGHT_GRAY);
            tfName.setText("pl.: Dr. Kiss Nagy Aladdin");
        }
    }//GEN-LAST:event_tfNevFocusLost

    private void tfMilitaryIdFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusGained
        tfMilitaryId.setForeground(Color.BLACK);
        if (tfMilitaryId.getText().trim().equals("12345678")) {
            tfMilitaryId.setText("");
        }
    }//GEN-LAST:event_tfSztszFocusGained

    private void tfMilitaryIdFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSztszFocusLost
        if (tfMilitaryId.getText().trim().isEmpty()) {
            tfMilitaryId.setForeground(Color.LIGHT_GRAY);
            tfMilitaryId.setText("12345678");
        }
    }//GEN-LAST:event_tfSztszFocusLost

    private void tfBirthDateFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSzulDatumFocusGained
        tfBirthDate.setForeground(Color.BLACK);
        if (tfBirthDate.getText().trim().equals("1900.01.01.")) {
            tfBirthDate.setText("");
        }
    }//GEN-LAST:event_tfSzulDatumFocusGained

    private void tfBirthDateFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSzulDatumFocusLost
        if (tfBirthDate.getText().trim().isEmpty()) {
            tfBirthDate.setForeground(Color.LIGHT_GRAY);
            tfBirthDate.setText("1900.01.01.");
        }
    }//GEN-LAST:event_tfSzulDatumFocusLost

    private void tfMothersMaidenNameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfAnyjaNeveFocusGained
        tfMothersMaidenName.setForeground(Color.BLACK);
        if (tfMothersMaidenName.getText().trim().equals("pl.: Dr. Kiss Nagy Aladdin")) {
            tfMothersMaidenName.setText("");
        }
    }//GEN-LAST:event_tfAnyjaNeveFocusGained

    private void tfMothersMaidenNameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfAnyjaNeveFocusLost
        if (tfMothersMaidenName.getText().trim().isEmpty()) {
            tfMothersMaidenName.setForeground(Color.LIGHT_GRAY);
            tfMothersMaidenName.setText("pl.: Dr. Kiss Nagy Aladdin");
        }
    }//GEN-LAST:event_tfAnyjaNeveFocusLost

    private void tfHeightFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfMagassagFocusGained
        tfHeight.setForeground(Color.BLACK);
        if (tfHeight.getText().trim().equals("pl.: 175")) {
            tfHeight.setText("");
        }
    }//GEN-LAST:event_tfMagassagFocusGained

    private void tfHeightFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfMagassagFocusLost
        if (tfHeight.getText().trim().isEmpty()) {
            tfHeight.setForeground(Color.LIGHT_GRAY);
            tfHeight.setText("pl.: 175");
        }
    }//GEN-LAST:event_tfMagassagFocusLost

    private void tfWeightFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSulyFocusGained
        tfWeight.setForeground(Color.BLACK);
        if (tfWeight.getText().trim().equals("pl.: 75")) {
            tfWeight.setText("");
        }
    }//GEN-LAST:event_tfSulyFocusGained

    private void tfWeightFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfSulyFocusLost
        if (tfWeight.getText().trim().isEmpty()) {
            tfWeight.setForeground(Color.LIGHT_GRAY);
            tfWeight.setText("pl.: 75");
        }
    }//GEN-LAST:event_tfSulyFocusLost

    private void tfBodyFatFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTestZsirFocusGained
        tfBodyFat.setForeground(Color.BLACK);
        if (tfBodyFat.getText().trim().equals("pl.: 10,5")) {
            tfBodyFat.setText("");
        }
    }//GEN-LAST:event_tfTestZsirFocusGained

    private void tfBodyFatFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfTestZsirFocusLost
        if (tfBodyFat.getText().trim().isEmpty()) {
            tfBodyFat.setForeground(Color.LIGHT_GRAY);
            tfBodyFat.setText("pl.: 10,5");
        }
    }//GEN-LAST:event_tfTestZsirFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cbUnit;
    private javax.swing.JComboBox<String> cbSex;
    private javax.swing.JComboBox<Rank> cbRank;
    private javax.swing.JLabel jLblName;
    private javax.swing.JLabel jLblSex;
    private javax.swing.JLabel jLblMilitaryId;
    private javax.swing.JLabel jLblBirthDate;
    private javax.swing.JLabel jLblMothersMaidenName;
    private javax.swing.JLabel jLblHeight;
    private javax.swing.JLabel jLblWeight;
    private javax.swing.JLabel jLblBodyFat;
    private javax.swing.JLabel jLblRank;
    private javax.swing.JLabel jLblUnit;
    private javax.swing.JButton jbCancel;
    private javax.swing.JButton jbOk;
    private javax.swing.JTextField tfMothersMaidenName;
    private javax.swing.JTextField tfHeight;
    private javax.swing.JTextField tfName;
    private javax.swing.JTextField tfWeight;
    private javax.swing.JTextField tfMilitaryId;
    private javax.swing.JTextField tfBirthDate;
    private javax.swing.JTextField tfBodyFat;
    // End of variables declaration//GEN-END:variables
}
