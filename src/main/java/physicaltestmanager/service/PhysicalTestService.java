package physicaltestmanager.service;

import java.sql.Connection;
import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.PhysicalTest;
import physicaltestmanager.test.dao.PhysicalTestRepository;
import physicaltestmanager.test.dao.impl.PhysicalTestRepositoryJDBCImpl;

/**
 *
 * @author
 */
public class PhysicalTestService {
    
    private final PhysicalTestRepository testRepo;
    
    public PhysicalTestService(Connection conn) throws PhysicalTestManagerDAOException {
        this.testRepo = new PhysicalTestRepositoryJDBCImpl(conn);
    }
    
    public List<PhysicalTest> findAll() throws PhysicalTestManagerDAOException {
        return testRepo.findAll();
    }

    public List<PhysicalTest> findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return testRepo.findByMilitaryId(militaryId);
    }

    public List<PhysicalTest> findByYear(int year) throws PhysicalTestManagerDAOException {
        return testRepo.findByYear(year);
    }
    
    public PhysicalTest findById(int id) throws PhysicalTestManagerDAOException {
		return testRepo.findById(id);
	}

    public void saveTest(PhysicalTest test) throws PhysicalTestManagerDAOException {
        testRepo.saveTest(test);
    }

    public void deleteTest(Integer id) throws PhysicalTestManagerDAOException {
        testRepo.deleteTest(id);
    }
}
