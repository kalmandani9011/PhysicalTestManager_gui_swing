package physicaltestmanager.service;

import java.sql.Connection;
import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.Person;
import physicaltestmanager.person.dao.PersonRepository;
import physicaltestmanager.person.dao.impl.PersonRepositoryJDBCImpl;

/**
 *
 * @author
 */
public class PersonService {
    
    private final PersonRepository personRepo;
    
    public PersonService(Connection conn) throws PhysicalTestManagerDAOException {
        this.personRepo = new PersonRepositoryJDBCImpl(conn);
    }

    public List<Person> findAll() throws PhysicalTestManagerDAOException {
        return personRepo.findAll();
    }
    
    public List<Person> findByName(String name) throws PhysicalTestManagerDAOException {
        return personRepo.findByName(name);
    }
    
    public Person findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return personRepo.findByMilitaryId(militaryId);
    }
    
    public Person findById(int id) throws PhysicalTestManagerDAOException {
        return personRepo.findById(id);
    }

    public void savePerson(Person person) throws PhysicalTestManagerDAOException {
        personRepo.savePerson(person);
    }

    public void deletePerson(Integer id) throws PhysicalTestManagerDAOException {
        personRepo.deletePerson(id);
    }
}
