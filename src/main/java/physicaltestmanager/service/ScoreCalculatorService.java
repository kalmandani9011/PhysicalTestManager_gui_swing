package physicaltestmanager.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.Person;
import physicaltestmanager.model.PhysicalTest;


/**
 * A service réteg központi osztálya, amely a többi service osztályon keresztül
 * végzi az adatbázisműveleteket, pontszámításokat.
 */
public class ScoreCalculatorService {

	private Connection conn;
    private PersonService personService;
    private PhysicalTestService testService;
    private FormOfMovementService movementService;
    
    public ScoreCalculatorService(Connection conn) throws PhysicalTestManagerDAOException, SQLException {
    	this.conn = conn;
    	this.personService = new PersonService(conn);
		this.testService = new PhysicalTestService(conn);
		this.movementService = new FormOfMovementService(conn);
    }
    
    public List<Person> getPersons() throws PhysicalTestManagerDAOException {
        return personService.findAll();
    }
    public List<PhysicalTest> getTests() throws PhysicalTestManagerDAOException {
    	return testService.findAll();
    }

    public List<Person> getPersonsWithTests() throws PhysicalTestManagerDAOException {
        List<Person> persons = getPersons();
        for (Person p : persons) {
            p.setTests(testService.findByMilitaryId(p.getMilitaryId()));
        }
        return persons;
    }
    
    public void savePerson(Person person) throws PhysicalTestManagerDAOException {
        personService.savePerson(person);
    }
    
    public Person findPersonByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return personService.findByMilitaryId(militaryId);
    }
    
    public Person findPersonById(int id) throws PhysicalTestManagerDAOException {
        return personService.findById(id);
    }
    
    public Person findPersonWithTestsById(int id) throws PhysicalTestManagerDAOException {
    	Person person = personService.findById(id);
    	refreshTest(person);
        return person;
    }
    
    public void deletePerson(Integer id) throws PhysicalTestManagerDAOException {
    	Person person = findPersonById(id);
    	for (PhysicalTest test : findTestByMilitaryId(person.getMilitaryId())) {
    		if (test.getMilitaryId() == person.getMilitaryId())
    			testService.deleteTest(test.getId());
    	}
        personService.deletePerson(id);
    }
    
    public List<PhysicalTest> findTestByYear(int year) throws PhysicalTestManagerDAOException {
        return testService.findByYear(year);
    }
    
    public List<PhysicalTest> findTestByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException {
        return testService.findByMilitaryId(militaryId);
    }
    
    public PhysicalTest findTestById(int id) throws PhysicalTestManagerDAOException {
        return testService.findById(id);
    }
    
    public void saveTestAndBindToPerson(Person person, PhysicalTest test) throws PhysicalTestManagerDAOException {
        calculateTestScoreAndQualification(person, test);
        testService.saveTest(test);
        refreshTest(person);
    }
    
    public void refreshTest(Person person) throws PhysicalTestManagerDAOException {
        List<PhysicalTest> tests = testService.findByMilitaryId(person.getMilitaryId());
        person.setTests(tests);
    }
    
    public String calculateTestScoreAndQualification(Person person, PhysicalTest test) throws PhysicalTestManagerDAOException {
        int score = movementService.calculateScore(person, test);
        test.setScore(score);
        test.setBmi(movementService.determineBmi(person));
        test.setBodyFatIndex(movementService.determineBodyFat(person));
        return movementService.getQualification();
    }
    
    public void deleteTest(Integer id) throws PhysicalTestManagerDAOException {
        testService.deleteTest(id);
    }
}
