package physicaltestmanager.model;

/**
 * Kar mozgásformák közös absztrakt osztálya.
 */
public abstract class ArmFormOfMovement {
    
    protected int maleAgeGroupOne;
    protected int femaleAgeGroupOne;
    protected int maleAgeGroupTwo;
    protected int femaleAgeGroupTwo;
    protected int maleAgeGroupThree;
    protected int femaleAgeGroupThree;
    protected int maleAgeGroupFour;
    protected int femaleAgeGroupFour;
    protected int maleAgeGroupFive;
    protected int femaleAgeGroupFive;
    protected int maleAgeGroupSix;
    protected int femaleAgeGroupSix;
    protected int maleAgeGroupSeven;
    protected int femaleAgeGroupSeven;
    protected int maleAgeGroupEight;
    protected int femaleAgeGroupEigth;
    protected int maleAgeGroupNine;
    protected int femaleAgeGroupNine;
    
    public ArmFormOfMovement(){}
    
    /**
     * Kizárólag a fekvőtámasz és a húzódzkodás mozgásformák előállítására.
     */
    public ArmFormOfMovement(int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo, int maleAgeGroupThree, int femaleAgeGroupThree,
            int maleAgeGroupFour, int femaleAgeGroupFour, int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix, int femaleAgeGroupSix, int maleAgeGroupSeven,
            int femaleAgeGroupSeven, int maleAgeGroupEight, int femaleAgeGroupEigth, int maleAgeGroupNine, int femaleAgeGroupNine) {
        this.maleAgeGroupOne = maleAgeGroupOne;
        this.femaleAgeGroupOne = femaleAgeGroupOne;
        this.maleAgeGroupTwo = maleAgeGroupTwo;
        this.femaleAgeGroupTwo = femaleAgeGroupTwo;
        this.maleAgeGroupThree = maleAgeGroupThree;
        this.femaleAgeGroupThree = femaleAgeGroupThree;
        this.maleAgeGroupFour = maleAgeGroupFour;
        this.femaleAgeGroupFour = femaleAgeGroupFour;
        this.maleAgeGroupFive = maleAgeGroupFive;
        this.femaleAgeGroupFive = femaleAgeGroupFive;
        this.maleAgeGroupSix = maleAgeGroupSix;
        this.femaleAgeGroupSix = femaleAgeGroupSix;
        this.maleAgeGroupSeven = maleAgeGroupSeven;
        this.femaleAgeGroupSeven = femaleAgeGroupSeven;
        this.maleAgeGroupEight = maleAgeGroupEight;
        this.femaleAgeGroupEigth = femaleAgeGroupEigth;
        this.maleAgeGroupNine = maleAgeGroupNine;
        this.femaleAgeGroupNine = femaleAgeGroupNine;
    }
    
    /**
     * Kizárólag a hajlított függés mozgásforma előállítására.
     */
    public ArmFormOfMovement(int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo, int maleAgeGroupThree, int femaleAgeGroupThree,
            int maleAgeGroupFour, int femaleAgeGroupFour, int maleAgeGroupFive, int femaleAgeGroupFive) {
        this.maleAgeGroupOne = maleAgeGroupOne;
        this.femaleAgeGroupOne = femaleAgeGroupOne;
        this.maleAgeGroupTwo = maleAgeGroupTwo;
        this.femaleAgeGroupTwo = femaleAgeGroupTwo;
        this.maleAgeGroupThree = maleAgeGroupThree;
        this.femaleAgeGroupThree = femaleAgeGroupThree;
        this.maleAgeGroupFour = maleAgeGroupFour;
        this.femaleAgeGroupFour = femaleAgeGroupFour;
        this.maleAgeGroupFive = maleAgeGroupFive;
        this.femaleAgeGroupFive = femaleAgeGroupFive;
    }
    
    
    /**Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupOne":
                return this.maleAgeGroupOne;
            case "maleAgeGroupTwo":
                return this.maleAgeGroupTwo;
            case "maleAgeGroupThree":
                return this.maleAgeGroupThree;
            case "maleAgeGroupFour":
                return this.maleAgeGroupFour;
            case "maleAgeGroupFive":
                return this.maleAgeGroupFive;
            case "maleAgeGroupSix":
                return this.maleAgeGroupSix;
            case "maleAgeGroupSeven":
                return this.maleAgeGroupSeven;
            case "maleAgeGroupEight":
                return this.maleAgeGroupEight;
            case "maleAgeGroupNine":
                return this.maleAgeGroupNine;
            case "femaleAgeGroupOne":
                return this.femaleAgeGroupOne;
            case "femaleAgeGroupTwo":
                return this.femaleAgeGroupTwo;
            case "femaleAgeGroupThree":
                return this.femaleAgeGroupThree;
            case "femaleAgeGroupFour":
                return this.femaleAgeGroupFour;
            case "femaleAgeGroupFive":
                return this.femaleAgeGroupFive;
            case "femaleAgeGroupSix":
                return this.femaleAgeGroupSix;
            case "femaleAgeGroupSeven":
                return this.femaleAgeGroupSeven;
            case "femaleAgeGroupEigth":
                return this.femaleAgeGroupEigth;
            case "noKorcsopKilenc":
                return this.femaleAgeGroupNine;
        }
        return 0;
    }

	public int getMaleAgeGroupOne() {
		return maleAgeGroupOne;
	}

	public void setMaleAgeGroupOne(int maleAgeGroupOne) {
		this.maleAgeGroupOne = maleAgeGroupOne;
	}

	public int getFemaleAgeGroupOne() {
		return femaleAgeGroupOne;
	}

	public void setFemaleAgeGroupOne(int femaleAgeGroupOne) {
		this.femaleAgeGroupOne = femaleAgeGroupOne;
	}

	public int getMaleAgeGroupTwo() {
		return maleAgeGroupTwo;
	}

	public void setMaleAgeGroupTwo(int maleAgeGroupTwo) {
		this.maleAgeGroupTwo = maleAgeGroupTwo;
	}

	public int getFemaleAgeGroupTwo() {
		return femaleAgeGroupTwo;
	}

	public void setFemaleAgeGroupTwo(int femaleAgeGroupTwo) {
		this.femaleAgeGroupTwo = femaleAgeGroupTwo;
	}

	public int getMaleAgeGroupThree() {
		return maleAgeGroupThree;
	}

	public void setMaleAgeGroupThree(int maleAgeGroupThree) {
		this.maleAgeGroupThree = maleAgeGroupThree;
	}

	public int getFemaleAgeGroupThree() {
		return femaleAgeGroupThree;
	}

	public void setFemaleAgeGroupThree(int femaleAgeGroupThree) {
		this.femaleAgeGroupThree = femaleAgeGroupThree;
	}

	public int getMaleAgeGroupFour() {
		return maleAgeGroupFour;
	}

	public void setMaleAgeGroupFour(int maleAgeGroupFour) {
		this.maleAgeGroupFour = maleAgeGroupFour;
	}

	public int getFemaleAgeGroupFour() {
		return femaleAgeGroupFour;
	}

	public void setFemaleAgeGroupFour(int femaleAgeGroupFour) {
		this.femaleAgeGroupFour = femaleAgeGroupFour;
	}

	public int getMaleAgeGroupFive() {
		return maleAgeGroupFive;
	}

	public void setMaleAgeGroupFive(int maleAgeGroupFive) {
		this.maleAgeGroupFive = maleAgeGroupFive;
	}

	public int getFemaleAgeGroupFive() {
		return femaleAgeGroupFive;
	}

	public void setFemaleAgeGroupFive(int femaleAgeGroupFive) {
		this.femaleAgeGroupFive = femaleAgeGroupFive;
	}

	public int getMaleAgeGroupSix() {
		return maleAgeGroupSix;
	}

	public void setMaleAgeGroupSix(int maleAgeGroupSix) {
		this.maleAgeGroupSix = maleAgeGroupSix;
	}

	public int getFemaleAgeGroupSix() {
		return femaleAgeGroupSix;
	}

	public void setFemaleAgeGroupSix(int femaleAgeGroupSix) {
		this.femaleAgeGroupSix = femaleAgeGroupSix;
	}

	public int getMaleAgeGroupSeven() {
		return maleAgeGroupSeven;
	}

	public void setMaleAgeGroupSeven(int maleAgeGroupSeven) {
		this.maleAgeGroupSeven = maleAgeGroupSeven;
	}

	public int getFemaleAgeGroupSeven() {
		return femaleAgeGroupSeven;
	}

	public void setFemaleAgeGroupSeven(int femaleAgeGroupSeven) {
		this.femaleAgeGroupSeven = femaleAgeGroupSeven;
	}

	public int getMaleAgeGroupEight() {
		return maleAgeGroupEight;
	}

	public void setMaleAgeGroupEight(int maleAgeGroupEight) {
		this.maleAgeGroupEight = maleAgeGroupEight;
	}

	public int getFemaleAgeGroupEight() {
		return femaleAgeGroupEigth;
	}

	public void setFemaleAgeGroupEight(int femaleAgeGroupEigth) {
		this.femaleAgeGroupEigth = femaleAgeGroupEigth;
	}

	public int getMaleAgeGroupNine() {
		return maleAgeGroupNine;
	}

	public void setMaleAgeGroupNine(int maleAgeGroupNine) {
		this.maleAgeGroupNine = maleAgeGroupNine;
	}

	public int getFemaleAgeGroupNine() {
		return femaleAgeGroupNine;
	}

	public void setFemaleAgeGroupNine(int femaleAgeGroupNine) {
		this.femaleAgeGroupNine = femaleAgeGroupNine;
	}
}
