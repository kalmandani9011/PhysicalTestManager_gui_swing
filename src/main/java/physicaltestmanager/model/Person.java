package physicaltestmanager.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * A személy adatai tárolására szolgáló osztály.
 */
public class Person {

    private Integer id;
    private int militaryId;
    private String name;
    private Rank rank;
    private String unit;
    private LocalDate birthDate;
    private String mothersMaidenName;
    private boolean male;
    private int height;
    private int weight;
    private double bodyFat;
    private List<PhysicalTest> tests;

    public Person() {}

    public Person(int militaryId, String name, Rank rank, String unit, LocalDate birthDate, String mothersMaidenName,
			boolean male, int height, int weight, double bodyFat) {
		this.militaryId = militaryId;
		this.name = name;
		this.rank = rank;
		this.unit = unit;
		this.birthDate = birthDate;
		this.mothersMaidenName = mothersMaidenName;
		this.male = male;
		this.height = height;
		this.weight = weight;
		this.bodyFat = bodyFat;
		this.tests = new ArrayList<>();
	}

    public Person(Integer id, int militaryId, String name, Rank rank, String unit, LocalDate birthDate,
			String mothersMaidenName, boolean male, int height, int weight, double bodyFat) {
		this.id = id;
		this.militaryId = militaryId;
		this.name = name;
		this.rank = rank;
		this.unit = unit;
		this.birthDate = birthDate;
		this.mothersMaidenName = mothersMaidenName;
		this.male = male;
		this.height = height;
		this.weight = weight;
		this.bodyFat = bodyFat;
		this.tests = new ArrayList<>();
	}

    /**
     * A személy korának meghatározása.
     */
    public int getAge() {
        if (birthDate != null) {
            return LocalDate.now().getYear() - birthDate.getYear();
        } else {
            return 0;
        }
    }

    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getMilitaryId() {
		return militaryId;
	}

	public void setMilitaryId(int militaryId) {
		this.militaryId = militaryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rank getRank() {
		return rank;
	}

	public void setRank(Rank rank) {
		this.rank = rank;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getMothersMaidenName() {
		return mothersMaidenName;
	}

	public void setMothersMaidenName(String mothersMaidenName) {
		this.mothersMaidenName = mothersMaidenName;
	}

	public boolean isMale() {
		return male;
	}

	public void setMale(boolean male) {
		this.male = male;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public double getBodyFat() {
		return bodyFat;
	}

	public void setBodyFat(double bodyFat) {
		this.bodyFat = bodyFat;
	}

	public List<PhysicalTest> getTests() {
		return tests;
	}

	public void setTests(List<PhysicalTest> test) {
		this.tests = test;
	}

	@Override
    public String toString() {
        if (rank != null)
            return String.format("%-4d", id) + "| " + militaryId + " | " + String.format("%-40s", name + " " + rank.getFullName());
        else 
        	return "";
    }
}
