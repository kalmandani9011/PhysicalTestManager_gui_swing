package physicaltestmanager.model;

/**
 *A BMI index meghatározásához szükséges osztály.
 */
public class BMI {

    private int height;
    private int maleAgeGroupOne;
    private int femaleAgeGroupOne;
    private int maleAgeGroupTwo;
    private int femaleAgeGroupTwo;
    private int maleAgeGroupThree;
    private int femaleAgeGroupThree;
    private int maleAgeGroupFour;
    private int femaleAgeGroupFour;
    private int minWeight;

    public BMI() {}

    public BMI(int height, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int minWeight) {
		this.height = height;
		this.maleAgeGroupOne = maleAgeGroupOne;
		this.femaleAgeGroupOne = femaleAgeGroupOne;
		this.maleAgeGroupTwo = maleAgeGroupTwo;
		this.femaleAgeGroupTwo = femaleAgeGroupTwo;
		this.maleAgeGroupThree = maleAgeGroupThree;
		this.femaleAgeGroupThree = femaleAgeGroupThree;
		this.maleAgeGroupFour = maleAgeGroupFour;
		this.femaleAgeGroupFour = femaleAgeGroupFour;
		this.minWeight = minWeight;
	}

	/**
     * Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupOne":
                return this.maleAgeGroupOne;
            case "maleAgeGroupTwo":
                return this.maleAgeGroupTwo;
            case "maleAgeGroupThree":
                return this.maleAgeGroupThree;
            case "maleAgeGroupFour":
                return this.maleAgeGroupFour;
            case "femaleAgeGroupOne":
                return this.femaleAgeGroupOne;
            case "femaleAgeGroupTwo":
                return this.femaleAgeGroupTwo;
            case "femaleAgeGroupThree":
                return this.femaleAgeGroupThree;
            case "femaleAgeGroupFour":
                return this.femaleAgeGroupFour;
        }
        return 0;
    }

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getMaleAgeGroupOne() {
		return maleAgeGroupOne;
	}

	public void setMaleAgeGroupOne(int maleAgeGroupOne) {
		this.maleAgeGroupOne = maleAgeGroupOne;
	}

	public int getFemaleAgeGroupOne() {
		return femaleAgeGroupOne;
	}

	public void setFemaleAgeGroupOne(int femaleAgeGroupOne) {
		this.femaleAgeGroupOne = femaleAgeGroupOne;
	}

	public int getMaleAgeGroupTwo() {
		return maleAgeGroupTwo;
	}

	public void setMaleAgeGroupTwo(int maleAgeGroupTwo) {
		this.maleAgeGroupTwo = maleAgeGroupTwo;
	}

	public int getFemaleAgeGroupTwo() {
		return femaleAgeGroupTwo;
	}

	public void setFemaleAgeGroupTwo(int femaleAgeGroupTwo) {
		this.femaleAgeGroupTwo = femaleAgeGroupTwo;
	}

	public int getMaleAgeGroupThree() {
		return maleAgeGroupThree;
	}

	public void setMaleAgeGroupThree(int maleAgeGroupThree) {
		this.maleAgeGroupThree = maleAgeGroupThree;
	}

	public int getFemaleAgeGroupThree() {
		return femaleAgeGroupThree;
	}

	public void setFemaleAgeGroupThree(int femaleAgeGroupThree) {
		this.femaleAgeGroupThree = femaleAgeGroupThree;
	}

	public int getMaleAgeGroupFour() {
		return maleAgeGroupFour;
	}

	public void setMaleAgeGroupFour(int maleAgeGroupFour) {
		this.maleAgeGroupFour = maleAgeGroupFour;
	}

	public int getFemaleAgeGroupFour() {
		return femaleAgeGroupFour;
	}

	public void setFemaleAgeGroupFour(int femaleAgeGroupFour) {
		this.femaleAgeGroupFour = femaleAgeGroupFour;
	}

	public int getMinWeight() {
		return minWeight;
	}

	public void setMinWeight(int minWeight) {
		this.minWeight = minWeight;
	}

}
