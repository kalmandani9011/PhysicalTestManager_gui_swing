package physicaltestmanager.model;

/**
 *A 2000 m-es futás mozgásforma osztálya.
 */
public class Running2000 extends CirculationFormOfMovement {

    private int time;

    public Running2000(){}

    public Running2000(int time, int maleAgeGroupSix, int femaleAgeGroupSix, int maleAgeGroupSeven, int femaleAgeGroupSeven,
			int maleAgeGroupEight, int femaleAgeGroupEigth, int maleAgeGroupNine, int femaleAgeGroupNine) {
		super(maleAgeGroupSix, femaleAgeGroupSix, maleAgeGroupSeven, femaleAgeGroupSeven, maleAgeGroupEight,
				femaleAgeGroupEigth, maleAgeGroupNine, femaleAgeGroupNine);
		this.time = time;
	}

	/**Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupSix":
                return this.maleAgeGroupSix;
            case "maleAgeGroupSeven":
                return this.maleAgeGroupSeven;
            case "maleAgeGroupEight":
                return this.maleAgeGroupEight;
            case "maleAgeGroupNine":
                return this.maleAgeGroupNine;
            case "femaleAgeGroupSix":
                return this.femaleAgeGroupSix;
            case "femaleAgeGroupSeven":
                return this.femaleAgeGroupSeven;
            case "femaleAgeGroupEigth":
                return this.femaleAgeGroupEigth;
            case "femaleAgeGroupNine":
                return this.femaleAgeGroupNine;
        }
        return 0;
    }

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}
    
}