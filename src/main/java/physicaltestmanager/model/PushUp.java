package physicaltestmanager.model;

/**
 *Fekvőtámasz mozgásforma osztálya.
 */
public class PushUp extends ArmFormOfMovement {

    private int repetition;
    
    public PushUp(){}
    
    public PushUp(int repetition, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix, int femaleAgeGroupSix,
			int maleAgeGroupSeven, int femaleAgeGroupSeven, int maleAgeGroupEight, int femaleAgeGroupEigth,
			int maleAgeGroupNine, int femaleAgeGroupNine) {
		super(maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree, femaleAgeGroupThree,
				maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive, maleAgeGroupSix, femaleAgeGroupSix,
				maleAgeGroupSeven, femaleAgeGroupSeven, maleAgeGroupEight, femaleAgeGroupEigth, maleAgeGroupNine,
				femaleAgeGroupNine);
		this.repetition = repetition;
	}

	public int getRepetition() {
        return repetition;
    }

    public void setRepetition(int repetition) {
        this.repetition = repetition;
    }
}
