package physicaltestmanager.model;

/**
 *A 3200 m-es futás mozgásforma osztálya.
 */
public class Running3200 extends CirculationFormOfMovement {
    
    private int time;

    public Running3200() {}
    
    public Running3200(int time, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int maleAgeGroupFive, int femaleAgeGroupFive) {
		super(maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree, femaleAgeGroupThree,
				maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive);
		this.time = time;
	}

	/**Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupOne":
                return this.maleAgeGroupOne;
            case "maleAgeGroupTwo":
                return this.maleAgeGroupTwo;
            case "maleAgeGroupThree":
                return this.maleAgeGroupThree;
            case "maleAgeGroupFour":
                return this.maleAgeGroupFour;
            case "maleAgeGroupFive":
                return this.maleAgeGroupFive;
            case "femaleAgeGroupOne":
                return this.femaleAgeGroupOne;
            case "femaleAgeGroupTwo":
                return this.femaleAgeGroupTwo;
            case "femaleAgeGroupThree":
                return this.femaleAgeGroupThree;
            case "femaleAgeGroupFour":
                return this.femaleAgeGroupFour;
            case "femaleAgeGroupFive":
                return this.femaleAgeGroupFive;
        }
        return 0;
    }

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

}