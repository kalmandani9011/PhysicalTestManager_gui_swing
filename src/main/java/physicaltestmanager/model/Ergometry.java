package physicaltestmanager.model;

/**
 *Ergometria mozgásforma osztálya.
 */
public class Ergometry extends CirculationFormOfMovement {
    
    private int wattKg;

    public Ergometry(){}
    
    public Ergometry(int wattKg, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix, int femaleAgeGroupSix,
			int maleAgeGroupSeven, int femaleAgeGroupSeven) {
		super(maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree, femaleAgeGroupThree,
				maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive, maleAgeGroupSix, femaleAgeGroupSix,
				maleAgeGroupSeven, femaleAgeGroupSeven);
		this.wattKg = wattKg;
	}

	/**Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupOne":
                return this.maleAgeGroupOne;
            case "maleAgeGroupTwo":
                return this.maleAgeGroupTwo;
            case "maleAgeGroupThree":
                return this.maleAgeGroupThree;
            case "maleAgeGroupFour":
                return this.maleAgeGroupFour;
            case "maleAgeGroupFive":
                return this.maleAgeGroupFive;
            case "maleAgeGroupSix":
                return this.maleAgeGroupSix;
            case "maleAgeGroupSeven":
                return this.maleAgeGroupSeven;
            case "femaleAgeGroupOne":
                return this.femaleAgeGroupOne;
            case "femaleAgeGroupTwo":
                return this.femaleAgeGroupTwo;
            case "femaleAgeGroupThree":
                return this.femaleAgeGroupThree;
            case "femaleAgeGroupFour":
                return this.femaleAgeGroupFour;
            case "femaleAgeGroupFive":
                return this.femaleAgeGroupFive;
            case "femaleAgeGroupSix":
                return this.femaleAgeGroupSix;
            case "femaleAgeGroupSeven":
                return this.femaleAgeGroupSeven;
        }
        return 0;
    }
    
    public int getWattKg() {
        return wattKg;
    }

    public void setWattKg(int wattKg) {
        this.wattKg = wattKg;
    }
}