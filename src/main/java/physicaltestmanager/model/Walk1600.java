package physicaltestmanager.model;

/**
 *Az 1600 m-es menet mozgásforma osztálya.
 */
public class Walk1600 extends CirculationFormOfMovement {
    
    private int time;

    public Walk1600(){}

    public Walk1600(int time, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo, int femaleAgeGroupTwo,
			int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour, int femaleAgeGroupFour,
			int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix, int femaleAgeGroupSix,
			int maleAgeGroupSeven, int femaleAgeGroupSeven, int maleAgeGroupEight, int femaleAgeGroupEigth,
			int maleAgeGroupNine, int femaleAgeGroupNine) {
		super(maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree, femaleAgeGroupThree,
				maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive, maleAgeGroupSix, femaleAgeGroupSix,
				maleAgeGroupSeven, femaleAgeGroupSeven, maleAgeGroupEight, femaleAgeGroupEigth, maleAgeGroupNine,
				femaleAgeGroupNine);
		this.time = time;
	}

	/**Visszaadja a korcsoportnak megfelelő értéket.
     * @param ageGroup Ez alapján azonosítja be a megfelelő korcsoportot.
     */
    @Override
    public int getAgeGroup(String ageGroup) {
        switch (ageGroup) {
            case "maleAgeGroupOne":
                return this.maleAgeGroupOne;
            case "maleAgeGroupTwo":
                return this.maleAgeGroupTwo;
            case "maleAgeGroupThree":
                return this.maleAgeGroupThree;
            case "maleAgeGroupFour":
                return this.maleAgeGroupFour;
            case "maleAgeGroupFive":
                return this.maleAgeGroupFive;
            case "maleAgeGroupSix":
                return this.maleAgeGroupSix;
            case "maleAgeGroupSeven":
                return this.maleAgeGroupSeven;
            case "maleAgeGroupEight":
                return this.maleAgeGroupEight;
            case "maleAgeGroupNine":
                return this.maleAgeGroupNine;
            case "femaleAgeGroupOne":
                return this.femaleAgeGroupOne;
            case "femaleAgeGroupTwo":
                return this.femaleAgeGroupTwo;
            case "femaleAgeGroupThree":
                return this.femaleAgeGroupThree;
            case "femaleAgeGroupFour":
                return this.femaleAgeGroupFour;
            case "femaleAgeGroupFive":
                return this.femaleAgeGroupFive;
            case "femaleAgeGroupSix":
                return this.femaleAgeGroupSix;
            case "femaleAgeGroupSeven":
                return this.femaleAgeGroupSeven;
            case "femaleAgeGroupEigth":
                return this.femaleAgeGroupEigth;
            case "femaleAgeGroupNine":
                return this.femaleAgeGroupNine;
        }
        return 0;
    }

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

}