package physicaltestmanager.model;

/**
 *A lapocka emelés mozgásforma osztálya.
 */
public class Crunch extends TrunkFormOfMovement {
    
    public Crunch(){}

	public Crunch(int repetition, int maleAgeGroupOne, int femaleAgeGroupOne, int maleAgeGroupTwo,
			int femaleAgeGroupTwo, int maleAgeGroupThree, int femaleAgeGroupThree, int maleAgeGroupFour,
			int femaleAgeGroupFour, int maleAgeGroupFive, int femaleAgeGroupFive, int maleAgeGroupSix,
			int femaleAgeGroupSix, int maleAgeGroupSeven, int femaleAgeGroupSeven, int maleAgeGroupEight,
			int femaleAgeGroupEigth, int maleAgeGroupNine, int femaleAgeGroupNine) {
		super(repetition, maleAgeGroupOne, femaleAgeGroupOne, maleAgeGroupTwo, femaleAgeGroupTwo, maleAgeGroupThree,
				femaleAgeGroupThree, maleAgeGroupFour, femaleAgeGroupFour, maleAgeGroupFive, femaleAgeGroupFive,
				maleAgeGroupSix, femaleAgeGroupSix, maleAgeGroupSeven, femaleAgeGroupSeven, maleAgeGroupEight,
				femaleAgeGroupEigth, maleAgeGroupNine, femaleAgeGroupNine);
	}
}