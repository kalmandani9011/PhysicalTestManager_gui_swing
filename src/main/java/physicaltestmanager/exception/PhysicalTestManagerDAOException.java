package physicaltestmanager.exception;

/**
 *
 * @author
 */
public class PhysicalTestManagerDAOException extends Exception {

    public PhysicalTestManagerDAOException() {
    }

    public PhysicalTestManagerDAOException(String message) {
        super(message);
    }

    public PhysicalTestManagerDAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public PhysicalTestManagerDAOException(Throwable cause) {
        super(cause);
    }
}
