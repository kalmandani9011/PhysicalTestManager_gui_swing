package physicaltestmanager.person.dao;

import java.util.List;

import physicaltestmanager.exception.PhysicalTestManagerDAOException;
import physicaltestmanager.model.Person;

/**
 * A személyeket tartalmazó fájlok, adatbázisok elérésére szolgáló osztályok közös interface-je.
 */
public interface PersonRepository {
    
    /**
     * Az összes személy lekérdezésére szolgáló metódus.
     *
     * @return Visszaadja az összes személyt tartalmazó listát.
     */
    List<Person> findAll() throws PhysicalTestManagerDAOException;
    
    /**
     * SZTSZ szám szerint megkeresi az adott személyt.
     *
     * @param militaryId Az SZTSZ szám, ami alapján keressük a személyt.
     * @return Visszaadja a személy objektumot.
     */
    Person findByMilitaryId(int militaryId) throws PhysicalTestManagerDAOException;
    
    /**
     * Sorszám szerint megkeresi az adott személyt.
     *
     * @param id A sorszám, ami alapján keressük a személyt.
     * @return Visszaadja a személy objektumot.
     */
    Person findById(int id) throws PhysicalTestManagerDAOException;
    
    /**
     * Név szerint megkeresi az adott személyt vagy személyeket.
     *
     * @param name A név, ami alapján keressük a személyt vagy személyeket.
     * @return Visszaadja a személyek listáját.
     */
    List<Person> findByName(String name) throws PhysicalTestManagerDAOException;
    
    /**
     * Elmenti az adatbázisba a személy objektumot. Ha nem létezik még a személy
     * az adatbázisban, akkor újat ad hozzá, ha létezik, akkor felülírja a
     * meglévőt.
     *
     * @param p A személy, akit el akarunk menteni az adatbázisba.
     */
    void savePerson(Person p) throws PhysicalTestManagerDAOException;
    
    /**
     * Új személy felvitele az adatbázisba.
     */
    void insertPerson(Person p) throws PhysicalTestManagerDAOException;
    
    /**
     * Személy adatainak frissítése az adatbázisban.
     */
    void updatePerson(Person p) throws PhysicalTestManagerDAOException;
    
    /**
     * Töröl egy személyt a megadott sorszám alapján az adatbázisból.
     */
    void deletePerson(int id) throws PhysicalTestManagerDAOException;
    
}
